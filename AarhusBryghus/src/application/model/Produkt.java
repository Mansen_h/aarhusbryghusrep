package application.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Produkt implements Serializable {

	private String navn;
	private int nr;
	private ProduktGruppe produktgruppe;

	public Produkt(String navn, int nr, ProduktGruppe p) {
		this.navn = navn;
		this.nr = nr;
		this.produktgruppe = p;
	}

	public double getPant() {
		return produktgruppe.getPant();
	}

	public String getNavn() {
		return navn;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void setProduktgruppe(ProduktGruppe produktgruppe) {
		if (this.produktgruppe != produktgruppe) {
			ProduktGruppe oldgruppe = this.produktgruppe;
			if (oldgruppe != null) {
				oldgruppe.removeProdukt(this);
			}
			if (produktgruppe != null) {
				this.produktgruppe = produktgruppe;
				produktgruppe.addProdukt(this);
			}
		}
	}

	@Override
	public String toString() {
		return nr + ": " + navn;
	}
	
	public ProduktGruppe getProduktGruppe() {
		return produktgruppe;
	}
}
