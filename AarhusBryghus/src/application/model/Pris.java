package application.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Pris implements Serializable {

	private double pris;
	private int klip;
	
	private Produkt produkt;
	private PrisListe prisListe;
	
	public Pris(double pris, int klip, Produkt produkt, PrisListe prisListe) {
		this.pris = pris;
		this.klip = klip;
		this.produkt = produkt;
		this.prisListe = prisListe;
	}
	
	public double getPant() {
		return produkt.getPant();
	}

	public double getPris() {
		return pris;
	}

	public int getKlip() {
		return klip;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public PrisListe getPrisListe() {
		return prisListe;
	}
	
	public void setPrisListe(PrisListe p) {
		if(prisListe!=p) {
			this.prisListe=p;
		}
	}
	
	public String toString() {
		return produkt.getNavn() +", Kr: "+ pris+ ", Klip: " + klip;
	}
}
