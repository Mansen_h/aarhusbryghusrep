package application.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@SuppressWarnings("serial")
public class ReservationsOrdre extends Ordre implements Serializable {

	private LocalTime reservationsTidspunkt;
	private LocalDate reservationsDato;

	public ReservationsOrdre(String beskrivelse, LocalTime reservationsTidspunkt, LocalDate reservationsDato) {
		super(beskrivelse);
		this.reservationsTidspunkt = reservationsTidspunkt;
		this.reservationsDato = reservationsDato;
	}

	public LocalTime getReservationsTidspunkt() {
		return reservationsTidspunkt;
	}

	public LocalDate getReservationsDato() {
		return reservationsDato;
	}
	
	public void setReservationsTidspunkt(LocalTime reservationsTidspunkt) {
		this.reservationsTidspunkt = reservationsTidspunkt;
	}

	public void setReservationsDato(LocalDate reservationsDato) {
		this.reservationsDato = reservationsDato;
	}

	@Override
	public String toString() {
		return super.toString() + " " + reservationsDato.toString() + " " + reservationsTidspunkt.getHour() + ":" + reservationsTidspunkt.getMinute();
	}
}
