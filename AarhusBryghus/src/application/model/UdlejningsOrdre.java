package application.model;

import java.io.Serializable;
import java.time.LocalDate;

@SuppressWarnings("serial")
public class UdlejningsOrdre extends Ordre implements Serializable {

	private LocalDate udlejningsDato;
	private LocalDate returDato;
	private boolean afleveret;

	public UdlejningsOrdre(String beskrivelse, LocalDate udlejningsDato, LocalDate returDato) {
		super(beskrivelse);
		this.udlejningsDato = udlejningsDato;
		this.returDato = returDato;
		afleveret = false;
	}

	@Override
	public double beregnRestBeloebPris() {
		return super.beregnRestBeloebPris();
	}

	public LocalDate getUdlejningsDato() {
		return udlejningsDato;
	}

	public LocalDate getReturDato() {
		return returDato;
	}
	
	public void setUdlejningsDato(LocalDate udlejningsDato) {
		this.udlejningsDato = udlejningsDato;
	}

	public void setReturDato(LocalDate returDato) {
		this.returDato = returDato;
	}

	@Override
	public String toString() {
		return super.toString() + " " + udlejningsDato.toString() + " - " + returDato.toString();
	}
	
	public boolean isAfleveret() {
		return afleveret;
	}
	
	public void setAfleveret(boolean afleveret) {
		this.afleveret = afleveret;
	}
}
