package application.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Ordrelinje implements Serializable {

	private int antal;
	private Pris pris;
	private Rabat rabat;

	public Ordrelinje(int antal, Pris pris) {
		this.antal = antal;
		this.pris = pris;
	}

	public double beregnPris() {
		double sum = pris.getPris() * antal;
		if (rabat != null) {
			sum = sum - rabat.beregnRabat(sum);
		}
		return sum;
	}

	public int beregnKlipPris() {
		int sum = pris.getKlip() * antal;
		if (rabat != null) {
			sum = (int) Math.ceil(sum - rabat.beregnRabat(sum));
		}
		return sum;
	}

	public double beregnPantPris() {
		return pris.getPant() * antal;
	}

	public int getAntal() {
		return antal;
	}

	public Pris getPris() {
		return pris;
	}
	
	public void setAntal(int antal) {
		this.antal = antal;
	}

	public void setPris(Pris pris) {
		this.pris = pris;
	}

	public Rabat createFastRabat(double rabat) {
		Rabat r = new FastRabat(rabat);
		this.rabat = r;
		return r;
	}

	public Rabat createProcentRabat(double procent) {
		Rabat r = new ProcentRabat(procent);
		this.rabat = r;
		return r;
	}

	public void setRabat(Rabat rabat) {
		if (this.rabat != rabat) {
			this.rabat = rabat;
		}
	}	

	@Override
	public String toString() {
		String txtString = "";
		if (pris != null) {
			txtString += antal + " x " + pris.getProdukt().getNavn() + ": ";
			if (pris.getPris() > 0) {
				txtString += pris.getPris() + " kr ";
			}
			if (pris.getKlip() > 0) {
				txtString += ", " + pris.getKlip() + " klip";
			}
			if (pris.getPant() > 0) {
				txtString += ", " + pris.getPant() + " pant";
			}
			if (rabat != null) {
				txtString += "\n\tRabat: " + rabat.toString();
			}

			if (antal > 1) {
				txtString += "\n\tSum: ";
				if (beregnPris() > 0) {
					txtString += beregnPris() + " kr ";
				}
				if (beregnKlipPris() > 0) {
					txtString += ", " + beregnKlipPris() + " klip";
				}
				if (beregnPantPris() > 0) {
					txtString += ", " + beregnPantPris() + " pant";
				}
			}
		}
		
		return txtString;
	}
}
