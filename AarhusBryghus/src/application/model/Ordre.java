package application.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Ordre implements Serializable {

	private String beskrivelse;
	private LocalDateTime oprettelsesTidspunkt; 
	private ArrayList<Ordrelinje> ordrelinjer;

	private ArrayList<Betaling>betalinger;

	public Ordre(String beskrivelse) {
		this.beskrivelse = beskrivelse;
		this.oprettelsesTidspunkt = LocalDateTime.now();
		ordrelinjer = new ArrayList<Ordrelinje>();
		betalinger = new ArrayList<Betaling>();
	}
	
	public double beregnTotalBeloebPris() {
		double sum = 0;
		for (int i = 0; i < ordrelinjer.size(); i++) {
			sum += ordrelinjer.get(i).beregnPris();
		}
		return sum;
	}

	public double beregnRestBeloebPris() {
		double sum = beregnTotalBeloebPris();
		for(int i = 0; i < betalinger.size(); i++) {
			sum -= betalinger.get(i).getBetaltBeloeb();
		}
		return sum;
	}

	public int beregnTotalKlipPris() {
		int sum = 0;
		for (int i = 0; i < ordrelinjer.size(); i++) {
			sum += ordrelinjer.get(i).beregnKlipPris();
		}
		return sum;
	}
	
	public int beregnRestKlipPris() {		
		int sum = 0;
		int i = 0;
		while (i < ordrelinjer.size() && sum != -1) {
			if (ordrelinjer.get(i).beregnKlipPris() > 0) {
				sum += ordrelinjer.get(i).beregnKlipPris();
			} else {
				sum = -1;
			}
			i++;
		}
		if (sum != -1) {
			for (int j = 0; j < betalinger.size(); j++) {
				sum -= betalinger.get(j).getAntalKlip();
			}
		} else {
			sum = 0;
		}
		
		return sum;
	}

	public double beregnPantPris() {
		double sum = 0;
		for (int i = 0; i < ordrelinjer.size(); i++) {
			sum += ordrelinjer.get(i).beregnPantPris();
		}
		return sum;
	}

	public ArrayList<Ordrelinje> getOrdrelinjer() {
		return new ArrayList<>(ordrelinjer);
	}

	/**
	 * Opretter ordrelinje på ordren
	 * PRE: pris != null og antal > 0
	 * @param pris objektet med prisen på det produkt der skal tilføjes til ordrelinjen
	 * @param antal hvor mange af valgte pris/produkt der købes
	 * @return ordrelinjen der er oprettet
	 */
	public Ordrelinje createOrdrelinje(Pris pris, int antal) {
		Ordrelinje ordrelinje = new Ordrelinje(antal, pris);
		ordrelinjer.add(ordrelinje);
		return ordrelinje;
	}

	public void deleteOrdrelije(Ordrelinje ordrelinje) {
		if (ordrelinjer.contains(ordrelinje)) {
			ordrelinjer.remove(ordrelinje);
		}
	}

	/**
	 * Opretter en betaling og tilføjer til ordren.
	 * PRE: betalingsmetode != null
	 * @param betaltBeloeb beloeb der er betalt i kr
	 * @param antalKlip hvis der er betalt med klip, hvor mange klip
	 * @param betalingsmetode den valgte betalingsmetode
	 * @return betalingen der er oprettet
	 */
	public Betaling createBetaling(double betaltBeloeb, int antalKlip, Betalingsmetode betalingsmetode) {
		Betaling betaling;
		if (betalingsmetode == Betalingsmetode.REGNING) {
			betaling = Betaling.betalRegning();
		} else if (betalingsmetode == Betalingsmetode.KLIPPEKORT) {
			betaling = Betaling.betalMedKlip(antalKlip);
		} else {
			betaling = Betaling.normalBetaling(betaltBeloeb, betalingsmetode);
		}
		betalinger.add(betaling);
		return betaling;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public LocalDateTime getOprettelsesTidspunkt() {
		return oprettelsesTidspunkt;
	}
	@Override
	public String toString() {
		return beskrivelse;
	}

	public ArrayList<Betaling> getBetalinger(){
		return new ArrayList<>(betalinger);
	}


}
