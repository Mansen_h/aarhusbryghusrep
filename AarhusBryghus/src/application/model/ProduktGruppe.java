package application.model;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class ProduktGruppe implements Serializable {

	private String navn;
	private double pant;
	private ArrayList<Produkt> produkter;

	public ProduktGruppe(String navn, double pant) {
		this.navn = navn;
		this.pant = pant;
		produkter = new ArrayList<Produkt>();
	}

	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<>(produkter);
	}

	public Produkt createProdukt(String navn, int nr) {
		Produkt produkt = new Produkt(navn, nr, this);
		produkter.add(produkt);
		return produkt;
	}

	public void addProdukt(Produkt produkt) {
		if (!produkter.contains(produkt)) {
			produkter.add(produkt);
			produkt.setProduktgruppe(this);
		}
	}

	public void removeProdukt(Produkt produkt) {
		if (produkter.contains(produkt)) {
			produkter.remove(produkt);
			produkt.setProduktgruppe(null);
		}
	}

	public String getNavn() {
		return navn;
	}

	public double getPant() {
		return pant;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void setPant(double pant) {
		this.pant = pant;
	}

	@Override
	public String toString() {
		return navn + " " + pant;
	}
}
