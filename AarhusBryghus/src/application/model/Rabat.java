package application.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class Rabat implements Serializable {

	private double rabat;
	
	public Rabat(double rabat) {
		this.rabat = rabat;
	}
	
	public abstract double beregnRabat(double beløb);

	public double getRabat() {
		return rabat;
	}

	public void setRabat(double rabat) {
		this.rabat = rabat;
	}
}
