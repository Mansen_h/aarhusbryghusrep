package application.model;

@SuppressWarnings("serial")
public class FastRabat extends Rabat{
	
	public FastRabat(double rabat) {
		super(rabat);
	}
	
	@Override
	public double beregnRabat(double beløb) {
		return super.getRabat();
	}
	
	@Override
	public String toString() {
		return "Fast rabat: " + super.getRabat() + " kr/klip";
	}
}
