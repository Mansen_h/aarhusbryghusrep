package application.model;

@SuppressWarnings("serial")
public class ProcentRabat extends Rabat{
	
	public ProcentRabat(double rabat) {
		super(rabat);
	}

	@Override
	public double beregnRabat(double beløb) {
		return super.getRabat() / 100.0 * beløb;
	}
	
	@Override
	public String toString() {
		return "Procent rabat: " + super.getRabat() + "%";
	}
}
