package application.model;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings("serial")
public class Betaling implements Serializable{

	private double betaltBeloeb;
	private int antalKlip;
	private Betalingsmetode betalingsMetode;
	private LocalDateTime betalingsdag;
	
	private Betaling(Betalingsmetode betalingsmetode) {
		this.betalingsMetode = betalingsmetode;
		this.betalingsdag=LocalDateTime.now();
	}
	
	public static Betaling normalBetaling(double betaltBeloeb, Betalingsmetode betalingsmetode) {
		Betaling betaling = new Betaling(betalingsmetode);
		betaling.setBetaltBeloeb(betaltBeloeb);
		return betaling;
	}
	
	public static Betaling betalMedKlip(int antalKlip) {
		Betaling betaling = new Betaling(Betalingsmetode.KLIPPEKORT);
		betaling.setAntalKlip(antalKlip);
		return betaling;
	}
	
	public static Betaling betalRegning() {
		Betaling betaling = new Betaling(Betalingsmetode.REGNING);
		return betaling;
	}

	public double getBetaltBeloeb() {
		return betaltBeloeb;
	}

	public void setBetaltBeloeb(double betaltBeloeb) {
		this.betaltBeloeb = betaltBeloeb;
	}

	public int getAntalKlip() {
		return antalKlip;
	}

	public void setAntalKlip(int antalKlip) {
		this.antalKlip = antalKlip;
	}

	public Betalingsmetode getBetalingsMetode() {
		return betalingsMetode;
	}
	
	public LocalDateTime getBetalingsdag() {
		return betalingsdag;
	}
}
