package application.model;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class PrisListe implements Serializable {

	private String navn;
	private String beskrivelse;

	private ArrayList<Pris> priser;

	public PrisListe(String navn, String beskrivelse) {
		this.navn = navn;
		this.beskrivelse = beskrivelse;
		this.priser = new ArrayList<>();
	}

	public ArrayList<Pris> getPriser() {
		return new ArrayList<Pris>(priser);
	}

	public Pris createPris(double pris, int klip, Produkt produkt) {
		Pris p = new Pris(pris, klip, produkt, this);
		priser.add(p);
		return p;
	}

	public void deletePris(Pris pris) {
		if (priser.contains(pris)) {
			priser.remove(pris);
			pris.setPrisListe(null);
		}
	}

	public String getNavn() {
		return navn;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public String toString() {
		return navn;
	}

}
