package application.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import application.model.Betaling;
import application.model.Betalingsmetode;
import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Pris;
import application.model.PrisListe;
import application.model.Produkt;
import application.model.ProduktGruppe;
import application.model.Rabat;
import application.model.ReservationsOrdre;
import application.model.UdlejningsOrdre;
import storage.Storage;

public class Controller {

	private Storage storage;
	private static Controller controller;

	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}

	private Controller() {
		storage = Storage.getStorage();
		InitStorage.initStorage(this);
	}

	public void loadStorage() {
		try (FileInputStream fileIn = new FileInputStream("storage.ser")) {
			try (ObjectInputStream in = new ObjectInputStream(fileIn);) {
				storage = (Storage) in.readObject();

				System.out.println("Storage loaded from file storage.ser.");
			} catch (ClassNotFoundException ex) {
				System.out.println("Error loading storage object.");
				throw new RuntimeException(ex);
			}
		} catch (IOException ex) {
			System.out.println("Error loading storage object.");
			// throw new RuntimeException(ex);
		}

	}

	public void saveStorage() {
		try (FileOutputStream fileOut = new FileOutputStream("storage.ser")) {
			try (ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
				out.writeObject(storage);
				System.out.println("Storage saved in file storage.ser.");
			}
		} catch (IOException ex) {
			System.out.println("Error saving storage object.");
			throw new RuntimeException(ex);
		}
	}

	// ------------------------Produkt
	// Metoder----------------------------------------------

	public Produkt opretProdukt(String navn, int nr, ProduktGruppe p) {
		Produkt produkt = p.createProdukt(navn, nr);
		return produkt;
	}

	public void updateProdukt(String navn, int nr, ProduktGruppe p, Produkt produkt) {
		produkt.setProduktgruppe(p);
		produkt.setNavn(navn);
		produkt.setNr(nr);
	}

	public void deleteProdukt(Produkt p) {
		p.setProduktgruppe(null);
	}

	// ------------------------ProduktGruppe
	// Metoder----------------------------------------------

	public ArrayList<ProduktGruppe> getProduktGruppe() {
		return storage.getProduktgruppe();
	}

	public ProduktGruppe opretProduktgruppe(String navn, double pant) {
		ProduktGruppe produktgruppe = new ProduktGruppe(navn, pant);
		storage.addProduktGruppe(produktgruppe);
		return produktgruppe;
	}

	public void updateProduktGruppe(String navn, double pant, ProduktGruppe p) {
		p.setNavn(navn);
		p.setPant(pant);
	}

	public void deleteProduktGruppe(ProduktGruppe p) {
		storage.removeProduktgruppe(p);
	}

	// -------------------------------PrisListe
	// Metoder---------------------------------------

	public PrisListe opretPrisliste(String navn, String beskrivelse) {
		PrisListe prisliste = new PrisListe(navn, beskrivelse);
		storage.addPrisliste(prisliste);
		return prisliste;
	}

	public void udpdatePrisliste(String navn, String beskrivelse, PrisListe prisliste) {
		prisliste.setNavn(navn);
		prisliste.setBeskrivelse(beskrivelse);
	}

	public void deletePrisliste(PrisListe prisliste) {
		storage.removePrisliste(prisliste);
	}

	public ArrayList<PrisListe> getPrisliste() {
		return storage.getPrisliste();
	}

	public ArrayList<ProduktGruppe> produktGrupperPaaPrisListe(PrisListe prisliste) {
		ArrayList<ProduktGruppe> produktGrupper = new ArrayList<ProduktGruppe>();
		for (Pris pris : prisliste.getPriser()) {
			ProduktGruppe produktGruppePaaPris = pris.getProdukt().getProduktGruppe();
			if (!produktGrupper.contains(produktGruppePaaPris)) {
				produktGrupper.add(produktGruppePaaPris);
			}
		}
		return produktGrupper;
	}

	public ArrayList<Pris> produktPaaPrisListeIGruppe(PrisListe prisliste, ProduktGruppe produktGruppe) {
		ArrayList<Pris> produkter = new ArrayList<Pris>();
		for (Pris pris : prisliste.getPriser()) {
			if (pris.getProdukt().getProduktGruppe().equals(produktGruppe)) {
				produkter.add(pris);
			}
		}
		return produkter;
	}

	// --------------------------------Ordre
	// Metoder--------------------------------------
	
	public Ordre opretOrdre(String besrkivelse) {
		Ordre o = new Ordre(besrkivelse);
		storage.addOdre(o);
		return o;
	}

	public ReservationsOrdre opretReservationsOrdre(String beskrivelse, LocalTime reservationsTidspunkt,
			LocalDate reservationsDato) {
		ReservationsOrdre ro = new ReservationsOrdre(beskrivelse, reservationsTidspunkt, reservationsDato);
		storage.addOdre(ro);
		return ro;
	}

	public UdlejningsOrdre opretUdlejningsOrdre(String beskrivelse, LocalDate udlejningsDato, LocalDate returDato) {
		UdlejningsOrdre uo = new UdlejningsOrdre(beskrivelse, udlejningsDato, returDato);
		storage.addOdre(uo);
		return uo;
	}
	
	public void sletOrdre(Ordre ordre) {
		System.out.println("Forsøger at slette");
		if (getOrdre().contains(ordre)) {
			storage.removeOdre(ordre);
			System.out.println("Slettet");
		}
	}
	
	public void opdaterUdlejning(UdlejningsOrdre udlejning, String beskrivelse, LocalDate udlejningsDato, LocalDate returDato, boolean afleveret) {
		udlejning.setBeskrivelse(beskrivelse);
		udlejning.setUdlejningsDato(udlejningsDato);
		udlejning.setReturDato(returDato);
		udlejning.setAfleveret(afleveret);
	}
	
	public void opdaterReservation(ReservationsOrdre reservation, String beskrivelse, LocalTime reservationsTidspunkt,
			LocalDate reservationsDato) {
		reservation.setBeskrivelse(beskrivelse);
		reservation.setReservationsDato(reservationsDato);
		reservation.setReservationsTidspunkt(reservationsTidspunkt);
	}

	public ArrayList<Ordre> getOrdre() {
		return storage.getOrdrer();
	}
	
	public Rabat createProcentRabat(Ordrelinje ordrelinje, double rabat) {
		Rabat r = ordrelinje.createProcentRabat(rabat);
		return r;
	}
	
	public Rabat createFastRabat(Ordrelinje ordrelinje, double rabat) {
		Rabat r = ordrelinje.createFastRabat(rabat);
		return r;
	}

	// --------------------------------Ordrelinje
	// Metoder--------------------------------------
	public Ordrelinje opretOrdreLinje(int antal, Pris pris, Ordre ordre) {
		Ordrelinje ordrelinje = ordre.createOrdrelinje(pris, antal);
		return ordrelinje;
	}

	// -----------------------------Betaling
	// Metoder-----------------------------------------

	public Betaling opretBetaling(double betaltBeloeb, int antalKlip, Betalingsmetode betalingsmetode, Ordre ordre) {
		Betaling b = ordre.createBetaling(betaltBeloeb, antalKlip, betalingsmetode);
		return b;
	}

	// -------------------------------Pris
	// Metoder------------------------------------------

	public Pris opretPris(double pris, int klip, Produkt produkt, PrisListe prisListe) {
		Pris p = prisListe.createPris(pris, klip, produkt);
		return p;
	}
	
	public void sletPris(Pris pris) {
		pris.setPrisListe(null);
	}

	// ------------------------------Statistik
	// Metoder------------------------------------------------

	private ArrayList<Ordre> ordreFraDato(LocalDate dato) {
		ArrayList<Ordre> fundneOrdre = new ArrayList<Ordre>();
		for (Ordre ordre : getOrdre()) {
			if (ordre.getOprettelsesTidspunkt().toLocalDate().equals(dato)) {
				fundneOrdre.add(ordre);
			}
		}

		return fundneOrdre;
	}

	public String ordreFraDatoString(LocalDate dato) {
		ArrayList<Ordre> fundneOrdre = ordreFraDato(dato);
		String output = "";
		for (Ordre ordre : fundneOrdre) {
			output += ordre + ":\n";
			for (Ordrelinje ordrelinje : ordre.getOrdrelinjer()) {
				output += " - " + ordrelinje + "\n";
			}
			output += "Total pris: " + ordre.beregnTotalBeloebPris() + " kr ";
			if (ordre.beregnTotalKlipPris() > 0) {
				output += "eller " + ordre.beregnTotalKlipPris() + " klip ";
			}
			if (ordre.beregnPantPris() > 0) {
				output += ordre.beregnPantPris() + " pant";
			}
			output += "\n";
			
			output += "Resterende pris: " + ordre.beregnRestBeloebPris() + " kr ";
			if (ordre.beregnRestKlipPris() > 0) {
				output += "eller " + ordre.beregnRestKlipPris() + " klip";
			}
			output += "\n\n";
		}

		return output;
	}

	// ------------------------------------------------------------------------------

	public ArrayList<Ordre> kommendeUdlejninger() {
		ArrayList<Ordre> list = new ArrayList<>();
		for (int i = 0; i < storage.getOrdrer().size(); i++) {
			Ordre o = storage.getOrdrer().get(i);
			if (o instanceof UdlejningsOrdre) {
				if (!((UdlejningsOrdre) o).getUdlejningsDato().isBefore(LocalDate.now())) {
					list.add(o);
				}
			}
		}
		return list;
	}

	public ArrayList<Ordre> ikkeAfleveretUdstyr() {
		ArrayList<Ordre> list = new ArrayList<>();
		for (int i = 0; i < storage.getOrdrer().size(); i++) {
			Ordre o = storage.getOrdrer().get(i);
			if (o instanceof UdlejningsOrdre) {
				if (((UdlejningsOrdre) o).getUdlejningsDato().isBefore(LocalDate.now())) {
					if (((UdlejningsOrdre) o).getReturDato().isAfter(LocalDate.now())) {
						if(((UdlejningsOrdre) o).isAfleveret()==false) {
							list.add(o);
						}
					}
				}
			}
		}
		return list;
	}

	public ArrayList<Ordre> kommendeReservationer() {
		ArrayList<Ordre> list = new ArrayList<>();
		for (int i = 0; i < storage.getOrdrer().size(); i++) {
			Ordre o = storage.getOrdrer().get(i);
			if (o instanceof ReservationsOrdre) {
				if (((ReservationsOrdre) o).getReservationsDato().isAfter(LocalDate.now())) {
					list.add(o);
				}
			}
		}
		return list;
	}

	// -----------------------------------------------------

	public ArrayList<Ordre> ikkeBetalteReservationer(){
		ArrayList<Ordre> list = new ArrayList<>();
		for (int i = 0; i < storage.getOrdrer().size(); i++) {
			Ordre o = storage.getOrdrer().get(i);
			if (o instanceof ReservationsOrdre) {
				LocalDate l1 = ((ReservationsOrdre) o).getReservationsDato();
				LocalTime l2 = ((ReservationsOrdre) o).getReservationsTidspunkt();
				if(l1.isEqual(LocalDate.now()) && l2.isBefore(LocalTime.now())) {
					if(o.beregnRestBeloebPris()>0) {
						list.add(o);
					}
				}
			}
		}
		return list;
	}
	
	public ArrayList<Ordre> ordreMangelBetaling(){
		ArrayList<Ordre> list = new ArrayList<>();
		ArrayList<Ordre> reservation = ikkeBetalteReservationer();
		ArrayList<Ordre> udlejning  = kommendeUdlejninger();
		ArrayList<Ordre> udstyr = ikkeAfleveretUdstyr();
		for (int i = 0; i < storage.getOrdrer().size(); i++) {
			Ordre o = storage.getOrdrer().get(i);
			if(!reservation.contains(o) && !udlejning.contains(o) && !udstyr.contains(o)) {
				if(o.beregnRestBeloebPris()>0) {
					list.add(o);
				}
			}
		}
		return list;
	}


	//-----------------------------------------------------

	public int klippekortSolgt(LocalDate start, LocalDate slut) {
		int antal = 0;
		for (int i = 0; i < getOrdre().size(); i++) {
			Ordre ordre = getOrdre().get(i);
			LocalDate ordreDato = ordre.getOprettelsesTidspunkt().toLocalDate();
			if (!ordreDato.isBefore(start) && !ordreDato.isAfter(slut)) {
				for (int j = 0; j < ordre.getOrdrelinjer().size(); j++) {
					Ordrelinje ordrelinje = ordre.getOrdrelinjer().get(j);
					ProduktGruppe p = ordrelinje.getPris().getProdukt().getProduktGruppe();
					if (p.getNavn().contentEquals("Klippekort")) {
						antal += ordrelinje.getAntal();
					}
				}
			}
		}
		return antal;
	}

	public int klippekortBrugt(LocalDate start, LocalDate slut) {
		int antal = 0;
		for (int i = 0; i < getOrdre().size(); i++) {
			Ordre ordre = getOrdre().get(i);
			for (int j = 0; j < ordre.getBetalinger().size(); j++) {
				Betaling b = ordre.getBetalinger().get(j);
				LocalDate betalingsDato = b.getBetalingsdag().toLocalDate();
				if (b.getBetalingsMetode() == Betalingsmetode.KLIPPEKORT) {
					if (!betalingsDato.isBefore(start) && !betalingsDato.isAfter(slut)) {
						antal += b.getAntalKlip();
					}
				}
			}
		}
		return antal;
	}



	//----------------------------------------------------------------------------------

	public void opdaterOrdrelinje(Ordrelinje ordrelinje, Pris pris, int antal) {
		ordrelinje.setAntal(antal);
		ordrelinje.setPris(pris);
	}

}
