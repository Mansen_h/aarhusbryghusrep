package application.controller;

import java.time.LocalDate;
import java.time.LocalTime;

import application.model.Betalingsmetode;
import application.model.Ordre;
import application.model.Pris;
import application.model.PrisListe;
import application.model.Produkt;
import application.model.ProduktGruppe;
import application.model.ReservationsOrdre;
import application.model.UdlejningsOrdre;

public class InitStorage {

	@SuppressWarnings("unused")
	public static void initStorage(Controller controller) {
		ProduktGruppe pg1 = controller.opretProduktgruppe("Klippekort", 0);
		ProduktGruppe pg2 = controller.opretProduktgruppe("Flaske", 0);
		ProduktGruppe pg3 = controller.opretProduktgruppe("Fadøl, 40 cl", 0);
		ProduktGruppe pg4 = controller.opretProduktgruppe("Spiritus", 0);
		ProduktGruppe pg5 = controller.opretProduktgruppe("Fustage", 200);
		ProduktGruppe pg6 = controller.opretProduktgruppe("Kulsyre", 1000);
		ProduktGruppe pg7 = controller.opretProduktgruppe("Malt", 0);
		ProduktGruppe pg8 = controller.opretProduktgruppe("Beklædning", 0);
		ProduktGruppe pg9 = controller.opretProduktgruppe("Anlæg", 0);
		ProduktGruppe pg10 = controller.opretProduktgruppe("Glas", 0);
		ProduktGruppe pg11 = controller.opretProduktgruppe("Sampakninger", 0);
		ProduktGruppe pg12 = controller.opretProduktgruppe("Rundvisning", 0);

		// Klippekort
		Produkt p1 = controller.opretProdukt("Klippekort, 4 klip", 1, pg1);
		// Flaske
		Produkt p2 = controller.opretProdukt("Klosterbryg", 2, pg2);
		Produkt p3 = controller.opretProdukt("Sweet Georgia Brown", 3, pg2);
		Produkt p4 = controller.opretProdukt("Extra Pilsner", 4, pg2);
		Produkt p5 = controller.opretProdukt("Celebration", 5, pg2);
		Produkt p6 = controller.opretProdukt("Forårsbryg", 6, pg2);
		Produkt p7 = controller.opretProdukt("India Pale Ale", 7, pg2);
		Produkt p8 = controller.opretProdukt("Julebryg", 8, pg2);
		Produkt p9 = controller.opretProdukt("Juletønden", 9, pg2);
		Produkt p10 = controller.opretProdukt("Old Strong Ale", 10, pg2);
		Produkt p11 = controller.opretProdukt("Fregatten Jylland", 11, pg2);
		Produkt p12 = controller.opretProdukt("Imperial Stout", 12, pg2);
		Produkt p13 = controller.opretProdukt("Tribute", 13, pg2);
		Produkt p14 = controller.opretProdukt("Black Monster", 14, pg2);
		// Fadøl, 40 cl
		Produkt p15 = controller.opretProdukt("Klosterbryg", 15, pg3);
		Produkt p16 = controller.opretProdukt("Jazz Classic", 16, pg3);
		Produkt p17 = controller.opretProdukt("Extra Pilsner", 17, pg3);
		Produkt p18 = controller.opretProdukt("Celebration", 18, pg3);
		Produkt p19 = controller.opretProdukt("Blondie", 19, pg3);
		Produkt p20 = controller.opretProdukt("Forårsbryg", 20, pg3);
		Produkt p21 = controller.opretProdukt("India Pale Ale", 21, pg3);
		Produkt p22 = controller.opretProdukt("Julebryg", 22, pg3);
		Produkt p23 = controller.opretProdukt("Imperial Stout", 23, pg3);
		Produkt p24 = controller.opretProdukt("Special", 24, pg3);
		// Spiritus
		Produkt p25 = controller.opretProdukt("Whisky 45% 50 cl rør", 25, pg4);
		Produkt p26 = controller.opretProdukt("Whisky 4 cl", 26, pg4);
		Produkt p27 = controller.opretProdukt("Whisky 43% 50 cl rør", 27, pg4);
		Produkt p28 = controller.opretProdukt("U/ egesplint", 28, pg4);
		Produkt p29 = controller.opretProdukt("M/ egesplint ", 29, pg4);
		Produkt p30 = controller.opretProdukt("2x Whisky glas + brikker", 30, pg4);
		Produkt p31 = controller.opretProdukt("Liquor of Aarhus", 31, pg4);
		Produkt p32 = controller.opretProdukt("Lyng gin 50 cl", 32, pg4);
		Produkt p33 = controller.opretProdukt("Lyng gin 4 cl", 33, pg4);
		// Fustage
		Produkt p34 = controller.opretProdukt("Klosterbryg, 20 liter", 34, pg5);
		Produkt p35 = controller.opretProdukt("Jazz Classic, 25 liter, 20 liter", 35, pg5);
		Produkt p36 = controller.opretProdukt("Extra Pilsner, 25 liter", 36, pg5);
		Produkt p37 = controller.opretProdukt("Celebration, 20 liter ", 37, pg5);
		Produkt p38 = controller.opretProdukt("Blondie, 25 liter", 38, pg5);
		Produkt p39 = controller.opretProdukt("Forårsbryg, 20 liter", 39, pg5);
		Produkt p40 = controller.opretProdukt("India Pale Ale, 20 liter", 40, pg5);
		Produkt p41 = controller.opretProdukt("Julebryg, 20 liter", 41, pg5);
		Produkt p42 = controller.opretProdukt("Imperial Stout, 20 liter", 42, pg5);
		// Kulsyre
		Produkt p43 = controller.opretProdukt("6 Kg", 43, pg6);
		Produkt p44 = controller.opretProdukt("4 Kg", 44, pg6);
		Produkt p45 = controller.opretProdukt("10 Kg", 45, pg6);
		// Malt
		Produkt p46 = controller.opretProdukt("25 Kg sæk", 46, pg7);
		// Beklædning
		Produkt p47 = controller.opretProdukt("T-shirt", 47, pg8);
		Produkt p48 = controller.opretProdukt("Polo", 48, pg8);
		Produkt p49 = controller.opretProdukt("Cap", 49, pg8);
		// Anlæg
		Produkt p50 = controller.opretProdukt("1-Hane", 50, pg9);
		Produkt p51 = controller.opretProdukt("2-Haner", 51, pg9);
		Produkt p52 = controller.opretProdukt("Bar med flere haner", 52, pg9);
		Produkt p53 = controller.opretProdukt("Levering", 53, pg9);
		Produkt p54 = controller.opretProdukt("Krus", 54, pg9);
		// Glas
		Produkt p55 = controller.opretProdukt("Uanset størrelse", 55, pg10);
		// Sampakninger
		Produkt p56 = controller.opretProdukt("Gaveæske 2 øl, 2 glas", 56, pg11);
		Produkt p57 = controller.opretProdukt("Gaveæske 4 øl", 57, pg11);
		Produkt p58 = controller.opretProdukt("Trækasse 6 øl", 58, pg11);
		Produkt p59 = controller.opretProdukt("Gavekurv 6 øl, 2 glas", 59, pg11);
		Produkt p60 = controller.opretProdukt("Trækasse 6 øl, 6 glas", 60, pg11);
		Produkt p61 = controller.opretProdukt("Trækasse 12 øl", 61, pg11);
		Produkt p62 = controller.opretProdukt("Papkasse 12 øl", 62, pg11);
		// Rundvisning
		Produkt p63 = controller.opretProdukt("Pr person dag", 63, pg12);
		
		PrisListe prisListeBar = controller.opretPrisliste("Fredagsbar", "Fredagsbaren ved Aarhus Bryghus");
		// Klippekort
		Pris prisBar1 = controller.opretPris(130, 0, p1, prisListeBar);
		// Flaske
		Pris prisBar2 = controller.opretPris(70, 2, p2, prisListeBar);
		Pris prisBar3 = controller.opretPris(70, 2, p3, prisListeBar);
		Pris prisBar4 = controller.opretPris(70, 2, p4, prisListeBar);
		Pris prisBar5 = controller.opretPris(70, 2, p5, prisListeBar);
		Pris prisBar6 = controller.opretPris(70, 2, p6, prisListeBar);
		Pris prisBar7 = controller.opretPris(70, 2, p7, prisListeBar);
		Pris prisBar8 = controller.opretPris(70, 2, p8, prisListeBar);
		Pris prisBar9 = controller.opretPris(70, 2, p9, prisListeBar);
		Pris prisBar10 = controller.opretPris(70, 2, p10, prisListeBar);
		Pris prisBar11 = controller.opretPris(70, 2, p11, prisListeBar);
		Pris prisBar12 = controller.opretPris(70, 2, p12, prisListeBar);
		Pris prisBar13 = controller.opretPris(70, 2, p13, prisListeBar);
		Pris prisBar14 = controller.opretPris(100, 3, p14, prisListeBar);
		// Fadøl
		Pris prisBar15 = controller.opretPris(38, 1, p15, prisListeBar);
		Pris prisBar16 = controller.opretPris(38, 1, p16, prisListeBar);
		Pris prisBar17 = controller.opretPris(38, 1, p17, prisListeBar);
		Pris prisBar18 = controller.opretPris(38, 1, p18, prisListeBar);
		Pris prisBar19 = controller.opretPris(38, 1, p19, prisListeBar);
		Pris prisBar20 = controller.opretPris(38, 1, p20, prisListeBar);
		Pris prisBar21 = controller.opretPris(38, 1, p21, prisListeBar);
		Pris prisBar22 = controller.opretPris(38, 1, p22, prisListeBar);
		Pris prisBar23 = controller.opretPris(38, 1, p23, prisListeBar);
		Pris prisBar24 = controller.opretPris(38, 1, p24, prisListeBar);
		// Spiritus
		Pris prisBar25 = controller.opretPris(599, 0, p25, prisListeBar);
		Pris prisBar26 = controller.opretPris(50, 0, p26, prisListeBar);
		Pris prisBar27 = controller.opretPris(499, 0, p27, prisListeBar);
		Pris prisBar28 = controller.opretPris(300, 0, p28, prisListeBar);
		Pris prisBar29 = controller.opretPris(350, 0, p29, prisListeBar);
		Pris prisBar30 = controller.opretPris(80, 0, p30, prisListeBar);
		Pris prisBar31 = controller.opretPris(175, 0, p31, prisListeBar);
		Pris prisBar32 = controller.opretPris(350, 0, p32, prisListeBar);
		Pris prisBar33 = controller.opretPris(40, 0, p33, prisListeBar);
		// Kulsyre
		Pris prisBar43 = controller.opretPris(400, 0, p43, prisListeBar);
		// Beklædning
		Pris prisBar47 = controller.opretPris(70, 0, p47, prisListeBar);
		Pris prisBar48 = controller.opretPris(100, 0, p48, prisListeBar);
		Pris prisBar49 = controller.opretPris(30, 0, p49, prisListeBar);
		// Sampakninger
		Pris prisBar56 = controller.opretPris(110, 0, p56, prisListeBar);
		Pris prisBar57 = controller.opretPris(140, 0, p56, prisListeBar);
		Pris prisBar58 = controller.opretPris(250, 0, p56, prisListeBar);
		Pris prisBar59 = controller.opretPris(250, 0, p56, prisListeBar);
		Pris prisBar60 = controller.opretPris(350, 0, p56, prisListeBar);
		Pris prisBar61 = controller.opretPris(410, 0, p56, prisListeBar);
		Pris prisBar62 = controller.opretPris(370, 0, p56, prisListeBar);

		PrisListe prisListeButik = controller.opretPrisliste("Butik", "Butikken ved Aarhus Bryghus");
		// Klippekort
		Pris prisButik1 = controller.opretPris(130, 0, p1, prisListeButik);
		// Flaske
		Pris prisButik2 = controller.opretPris(36, 2, p2, prisListeButik);
		Pris prisButik3 = controller.opretPris(36, 2, p3, prisListeButik);
		Pris prisButik4 = controller.opretPris(36, 2, p4, prisListeButik);
		Pris prisButik5 = controller.opretPris(36, 2, p5, prisListeButik);
		Pris prisButik6 = controller.opretPris(36, 2, p6, prisListeButik);
		Pris prisButik7 = controller.opretPris(36, 2, p7, prisListeButik);
		Pris prisButik8 = controller.opretPris(36, 2, p8, prisListeButik);
		Pris prisButik9 = controller.opretPris(36, 2, p9, prisListeButik);
		Pris prisButik10 = controller.opretPris(36, 2, p10, prisListeButik);
		Pris prisButik11 = controller.opretPris(36, 2, p11, prisListeButik);
		Pris prisButik12 = controller.opretPris(36, 2, p12, prisListeButik);
		Pris prisButik13 = controller.opretPris(36, 2, p13, prisListeButik);
		Pris prisButik14 = controller.opretPris(60, 3, p14, prisListeButik);
		// Spiritus
		Pris prisButik25 = controller.opretPris(599, 0, p25, prisListeButik);
		Pris prisButik27 = controller.opretPris(499, 0, p27, prisListeButik);
		Pris prisButik28 = controller.opretPris(300, 0, p28, prisListeButik);
		Pris prisButik29 = controller.opretPris(350, 0, p29, prisListeButik);
		Pris prisButik30 = controller.opretPris(80, 0, p30, prisListeButik);
		Pris prisButik31 = controller.opretPris(175, 0, p31, prisListeButik);
		Pris prisButik32 = controller.opretPris(350, 0, p32, prisListeButik);
		// Fustage
		Pris prisButik34 = controller.opretPris(775, 0, p34, prisListeButik);
		Pris prisButik35 = controller.opretPris(625, 0, p35, prisListeButik);
		Pris prisButik36 = controller.opretPris(575, 0, p36, prisListeButik);
		Pris prisButik37 = controller.opretPris(775, 0, p37, prisListeButik);
		Pris prisButik38 = controller.opretPris(700, 0, p38, prisListeButik);
		Pris prisButik39 = controller.opretPris(775, 0, p39, prisListeButik);
		Pris prisButik40 = controller.opretPris(775, 0, p40, prisListeButik);
		Pris prisButik41 = controller.opretPris(775, 0, p41, prisListeButik);
		Pris prisButik42 = controller.opretPris(775, 0, p42, prisListeButik);
		// Kulsyre
		Pris prisButik43 = controller.opretPris(400, 0, p43, prisListeButik);
		// Malt
		Pris prisButik46 = controller.opretPris(300, 0, p46, prisListeButik);

		// Beklædning
		Pris prisButik47 = controller.opretPris(70, 0, p47, prisListeButik);
		Pris prisButik48 = controller.opretPris(100, 0, p48, prisListeButik);
		Pris prisButik49 = controller.opretPris(30, 0, p49, prisListeButik);
		// Anlæg
		Pris prisButik50 = controller.opretPris(250, 0, p50, prisListeButik);
		Pris prisButik51 = controller.opretPris(400, 0, p51, prisListeButik);
		Pris prisButik52 = controller.opretPris(500, 0, p52, prisListeButik);
		Pris prisButik53 = controller.opretPris(500, 0, p53, prisListeButik);
		Pris prisButik54 = controller.opretPris(60, 0, p54, prisListeButik);
		// Glas
		Pris prisButik55 = controller.opretPris(15, 0, p54, prisListeButik);
		// Sampakninger
		Pris prisButik56 = controller.opretPris(110, 0, p56, prisListeButik);
		Pris prisButik57 = controller.opretPris(140, 0, p57, prisListeButik);
		Pris prisButik58 = controller.opretPris(250, 0, p58, prisListeButik);
		Pris prisButik59 = controller.opretPris(250, 0, p59, prisListeButik);
		Pris prisButik60 = controller.opretPris(350, 0, p60, prisListeButik);
		Pris prisButik61 = controller.opretPris(410, 0, p61, prisListeButik);
		Pris prisButik62 = controller.opretPris(370, 0, p62, prisListeButik);
		// Rundvisning
		Pris prisButik63 = controller.opretPris(100, 0, p63, prisListeButik);
		
		Ordre ordreTest = controller.opretOrdre("Salgs testordre");
		controller.opretOrdreLinje(5, prisBar15, ordreTest);
		controller.opretOrdreLinje(1, prisBar16, ordreTest);
		controller.opretOrdreLinje(3, prisBar17, ordreTest);
		controller.opretOrdreLinje(10, prisBar18, ordreTest);
		
		Ordre ordreTest2 = controller.opretOrdre("Salgs testordre2");
		controller.opretOrdreLinje(300, prisBar2, ordreTest2);
		controller.opretOrdreLinje(18, prisBar3, ordreTest2);
		
		UdlejningsOrdre udlejningTest = controller.opretUdlejningsOrdre("Udlejning test", LocalDate.now().minusDays(1), LocalDate.now().plusDays(5));
		controller.opretOrdreLinje(5, prisButik35, udlejningTest);
		controller.opretBetaling(1000, 0, Betalingsmetode.DANKORT, udlejningTest); //Pant betaling
		
		ReservationsOrdre reservationTest = controller.opretReservationsOrdre("Reservation test", LocalTime.now().minusHours(5), LocalDate.now());
		controller.opretOrdreLinje(10, prisButik63, reservationTest);
		
	
		
	}
}
