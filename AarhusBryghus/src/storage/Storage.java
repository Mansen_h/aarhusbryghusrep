package storage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import application.model.Ordre;
import application.model.PrisListe;
import application.model.ProduktGruppe;

@SuppressWarnings("serial")
public class Storage implements Serializable {

	private static Storage storage;
	private List<ProduktGruppe> produktgrupper;
	private List<PrisListe> prislister;
	private List<Ordre> ordrer;

	public static Storage getStorage() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	private Storage() {
		produktgrupper = new ArrayList<>();
		prislister = new ArrayList<>();
		ordrer = new ArrayList<>();
	}

	public ArrayList<ProduktGruppe> getProduktgruppe() {
		return new ArrayList<>(produktgrupper);
	}

	public void addProduktGruppe(ProduktGruppe p) {
		produktgrupper.add(p);
	}

	public void removeProduktgruppe(ProduktGruppe p) {
		produktgrupper.remove(p);
	}

	public ArrayList<PrisListe> getPrisliste() {
		return new ArrayList<>(prislister);
	}

	public void addPrisliste(PrisListe p) {
		prislister.add(p);
	}

	public void removePrisliste(PrisListe p) {
		prislister.remove(p);
	}

	public ArrayList<Ordre> getOrdrer() {
		return new ArrayList<>(ordrer);
	}

	public void addOdre(Ordre o) {
		ordrer.add(o);
	}

	public void removeOdre(Ordre o) {
		ordrer.remove(o);
	}

}
