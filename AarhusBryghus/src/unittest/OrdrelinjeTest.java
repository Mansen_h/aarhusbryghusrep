package unittest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Pris;
import application.model.PrisListe;
import application.model.Produkt;
import application.model.ProduktGruppe;

public class OrdrelinjeTest {

	private Pris pris;
	private ProduktGruppe p1;
	private Produkt produkt;
	private PrisListe pl1;
	private Ordre ordre;

	@Before
	public void setUp() throws Exception {
		p1 = new ProduktGruppe("fadøl", 20);
		produkt = p1.createProdukt("Classic", 1);
		pl1 = new PrisListe("Bar", "fredagsbar");
		pris = new Pris(38, 2, produkt, pl1);
		ordre = new Ordre("o1");
	}

	@Test
	public void tc1() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 1);
		assertEquals(ol.beregnPris(), 38, 0);
	}

	@Test
	public void tc2() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 10);
		assertEquals(ol.beregnPris(), 380, 0);
	}

	@Test
	public void tc3() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 1);
		ol.createFastRabat(30);
		assertEquals(ol.beregnPris(), 8, 0);
	}

	@Test
	public void tc4() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 0);
		assertEquals(ol.beregnPris(), 0, 0);
	}

	@Test
	public void tc5() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, -10);
		assertEquals(ol.beregnPris(), -380, 0);
	}

	@Test(expected = NullPointerException.class)
	public void tc6() {
		Ordrelinje ol = ordre.createOrdrelinje(null, 1);
		assertEquals(ol.beregnPris(), null);
	}

	@Test
	public void tc7() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 1);
		assertEquals(ol.beregnKlipPris(), 2, 0);
	}

	@Test
	public void tc8() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 10);
		assertEquals(ol.beregnKlipPris(), 20, 0);
	}

	@Test
	public void tc9() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 1);
		ol.createFastRabat(1);
		assertEquals(ol.beregnKlipPris(), 1, 0);
	}

	@Test
	public void tc10() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 0);
		assertEquals(ol.beregnKlipPris(), 0, 0);
	}

	@Test
	public void tc11() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, -10);
		assertEquals(ol.beregnKlipPris(), -20, 0);
	}

	@Test(expected = NullPointerException.class)
	public void tc12() {
		Ordrelinje ol = ordre.createOrdrelinje(null, 1);
		assertEquals(ol.beregnKlipPris(), null);
	}

	@Test
	public void tc13() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 1);
		assertEquals(ol.beregnPantPris(), 20, 0);
	}

	@Test
	public void tc14() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 10);
		assertEquals(ol.beregnPantPris(), 200, 0);
	}

	@Test
	public void tc15() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, 0);
		assertEquals(ol.beregnPantPris(), 0, 0);
	}

	@Test
	public void tc16() {
		Ordrelinje ol = ordre.createOrdrelinje(pris, -10);
		assertEquals(ol.beregnPantPris(), -200, 0);
	}
}
