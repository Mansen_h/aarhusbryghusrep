package unittest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.Produkt;
import application.model.ProduktGruppe;

public class ProduktGruppeTest {

	private ProduktGruppe p1;
	private ProduktGruppe p2;
	private Produkt prod1;
	private Produkt prod2;

	@Before
	public void setUp() throws Exception {
		p1 = new ProduktGruppe("fadøl", 20);
		p2 = new ProduktGruppe("Bajer", 0);
		prod1 = new Produkt("Classic", 1, p1);
		prod2 = new Produkt("Pilsner", 2, p2);
		p2.addProdukt(prod2);
	}

	@Test
	public void tc1() {
		ProduktGruppe pg = new ProduktGruppe("Fadøl", 0);
		assertEquals(pg.getNavn(), "Fadøl");
		assertEquals(pg.getPant(), 0, 0);
	}

	@Test
	public void tc2() {
		ProduktGruppe pg = new ProduktGruppe("Fadøl", 100);
		assertEquals(pg.getNavn(), "Fadøl");
		assertEquals(pg.getPant(), 100, 0);
	}

	@Test
	public void tc3() {
		ProduktGruppe pg = new ProduktGruppe("Fadøl", -1);
		assertEquals(pg.getNavn(), "Fadøl");
		assertEquals(pg.getPant(), -1, 0);
	}

	@Test
	public void tc4() {
		ProduktGruppe pg = new ProduktGruppe("Fadøl", -100);
		assertEquals(pg.getNavn(), "Fadøl");
		assertEquals(pg.getPant(), -100, 0);
	}

	@Test
	public void tc5() {
		Produkt p = p1.createProdukt("Classic", 1);
		assertEquals(p1.getProdukter().contains(p), true);
		assertEquals(p1.getProdukter().get(0).getNavn(), "Classic");
		assertEquals(p1.getProdukter().get(0).getNr(), 1, 0);
	}

	@Test
	public void tc6() {
		Produkt p = p1.createProdukt("Pilsner", 111);
		assertEquals(p1.getProdukter().contains(p), true);
		assertEquals(p1.getProdukter().get(0).getNavn(), "Pilsner");
		assertEquals(p1.getProdukter().get(0).getNr(), 111, 0);
	}

	@Test
	public void tc7() {
		Produkt p = p1.createProdukt(null, 111);
		assertEquals(p1.getProdukter().contains(p), true);
		assertEquals(p1.getProdukter().get(0).getNavn(), null);
		assertEquals(p1.getProdukter().get(0).getNr(), 111, 0);
	}

	@Test
	public void tc8() {
		Produkt p = p1.createProdukt("Classic", 0);
		assertEquals(p1.getProdukter().contains(p), true);
		assertEquals(p1.getProdukter().get(0).getNavn(), "Classic");
		assertEquals(p1.getProdukter().get(0).getNr(), 0, 0);
	}

	@Test
	public void tc9() {
		Produkt p = p1.createProdukt("Classic", 9);
		assertEquals(p1.getProdukter().contains(p), true);
		assertEquals(p1.getProdukter().get(0).getNavn(), "Classic");
		assertEquals(p1.getProdukter().get(0).getNr(), 9, 0);
	}

	@Test
	public void tc10() {
		p1.addProdukt(prod1);
		assertEquals(p1.getProdukter().contains(prod1), true);
	}

	@Test
	public void tc11() {
		p2.addProdukt(prod2);
		assertEquals(p2.getProdukter().size(), 1);
		assertEquals(p2.getProdukter().get(0).getNavn(), "Pilsner");
	}

	@Test(expected = NullPointerException.class)
	public void tc12() {
		p2.addProdukt(null);
		assertEquals(p2.getProdukter().get(0), null);
	}
}
