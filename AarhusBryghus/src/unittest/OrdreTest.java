package unittest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import application.model.Betaling;
import application.model.Betalingsmetode;
import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Pris;
import application.model.PrisListe;
import application.model.Produkt;
import application.model.ProduktGruppe;

public class OrdreTest {

	private Pris pris;
	private ProduktGruppe p1;
	private Produkt produkt;
	private PrisListe pl1;
	private Ordre ordre;

	@Before
	public void setUp() throws Exception {
		p1 = new ProduktGruppe("fadøl", 0);
		produkt = p1.createProdukt("Classic", 1);
		pl1 = new PrisListe("Bar", "fredagsbar");
		pris = new Pris(38, 2, produkt, pl1);
		ordre = new Ordre("o1");
	}

	@Test
	public void tc1() {
		Ordrelinje ol1 = ordre.createOrdrelinje(pris, 1);
		System.out.println(ol1);
		assertEquals(ol1.getAntal(), 1);
		assertEquals(ol1.getPris(), pris);
	}

	@Test
	public void tc2() {
		Ordrelinje ol1 = ordre.createOrdrelinje(pris, 10);
		System.out.println(ol1);
		assertEquals(ol1.getAntal(), 10);
		assertEquals(ol1.getPris(), pris);
	}

	@Test
	public void tc3() {
		Ordrelinje ol1 = ordre.createOrdrelinje(pris, 0);
		System.out.println(ol1);
		assertEquals(ol1.getAntal(), 0);
		assertEquals(ol1.getPris(), pris);
	}

	@Test
	public void tc4() {
		Ordrelinje ol1 = ordre.createOrdrelinje(pris, -10);
		System.out.println(ol1);
		assertEquals(ol1.getAntal(), -10);
		assertEquals(ol1.getPris(), pris);
	}

	public void tc5() {
		Ordrelinje ol1 = ordre.createOrdrelinje(null, 1);
		System.out.println(ol1);
		assertEquals(ol1.getAntal(), 1);
		assertEquals(ol1.getPris(), null);
	}

	@Test
	public void tc6() {
		Ordrelinje ol1 = ordre.createOrdrelinje(pris, 1);
		System.out.println(ol1);
		ordre.deleteOrdrelije(ol1);
		assertEquals(ordre.getOrdrelinjer().size(), 0);
	}

	public void tc7() {
		Ordrelinje ol1 = ordre.createOrdrelinje(null, 0);
		System.out.println(ol1);
		ordre.deleteOrdrelije(null);
		assertEquals(ordre.getOrdrelinjer().size(), 1);
	}

	@Test
	public void tc8() {
		Ordre ordre1 = new Ordre("o2");
		Ordre ordre2 = new Ordre("o1");
		Ordrelinje ol1 = ordre1.createOrdrelinje(pris, 1);
		ordre2.createOrdrelinje(pris, 1);
		ordre2.deleteOrdrelije(ol1);
		assertEquals(ordre2.getOrdrelinjer().size(), 1);
	}

	@Test
	public void tc9() {
		Betaling b = ordre.createBetaling(1, 0, Betalingsmetode.KONTANT);
		System.out.println(ordre.getBetalinger());
		assertEquals(ordre.getBetalinger().contains(b), true);
		assertEquals(ordre.getBetalinger().get(0).getBetaltBeloeb(), 1, 0);
	}

	@Test
	public void tc10() {
		Betaling b = ordre.createBetaling(100, 0, Betalingsmetode.KONTANT);
		System.out.println(b);
		assertEquals(ordre.getBetalinger().contains(b), true);
		assertEquals(ordre.getBetalinger().get(0).getBetaltBeloeb(), 100, 0);
	}

	@Test
	public void tc11() {
		Betaling b = ordre.createBetaling(0, 1, Betalingsmetode.KLIPPEKORT);
		assertEquals(ordre.getBetalinger().contains(b), true);
		assertEquals(ordre.getBetalinger().get(0).getAntalKlip(), 1);
	}

	@Test
	public void tc12() {
		Betaling b = ordre.createBetaling(0, 5, Betalingsmetode.KLIPPEKORT);
		assertEquals(ordre.getBetalinger().contains(b), true);
		assertEquals(ordre.getBetalinger().get(0).getAntalKlip(), 5);
	}

	@Test
	public void tc13() {
		Betaling b = ordre.createBetaling(0, 0, Betalingsmetode.REGNING);
		assertEquals(ordre.getBetalinger().contains(b), true);
		assertEquals(ordre.getBetalinger().get(0).getBetalingsMetode(), Betalingsmetode.REGNING);
	}

	@Test
	public void tc14() {
		Betaling b = ordre.createBetaling(-1, 0, Betalingsmetode.KONTANT);
		assertEquals(ordre.getBetalinger().contains(b), true);
		assertEquals(ordre.getBetalinger().get(0).getBetaltBeloeb(), -1, 0);
	}

	@Test
	public void tc15() {
		Betaling b = ordre.createBetaling(0, -1, Betalingsmetode.KLIPPEKORT);
		assertEquals(ordre.getBetalinger().contains(b), true);
		assertEquals(ordre.getBetalinger().get(0).getAntalKlip(), -1, 0);
	}
}
