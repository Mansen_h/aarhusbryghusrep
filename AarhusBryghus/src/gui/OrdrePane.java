package gui;

import java.time.LocalDate;
import java.time.LocalTime;

import application.controller.Controller;
import application.model.Ordre;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class OrdrePane extends GridPane {

	private Controller controller;

	public OrdrePane() {
		controller = Controller.getController();
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		initContent();
		this.setAlignment(Pos.CENTER);
		
	}

	private void initContent() {

		Button btnSalg = new Button("Salg");
		this.add(btnSalg, 0, 0);
		btnSalg.setMaxWidth(Double.MAX_VALUE);
		btnSalg.setStyle("-fx-font-size: 2em; ");
		btnSalg.setOnAction(event -> salgsAction());
		
		Button btnRundvisning = new Button("Rundvisning");
		this.add(btnRundvisning, 1, 0);
		btnRundvisning.setMaxWidth(Double.MAX_VALUE);;
		btnRundvisning.setStyle("-fx-font-size: 2em; ");
		btnRundvisning.setOnAction(event -> rundvisningAction());

		Button btnUdlejning = new Button("Udlejning");
		this.add(btnUdlejning, 2, 0);
		btnUdlejning.setMaxWidth(Double.MAX_VALUE);
		btnUdlejning.setStyle("-fx-font-size: 2em; ");
		btnUdlejning.setOnAction(event -> udlejningsAction());
	}

	private void salgsAction() {
		Ordre salg = controller.opretOrdre("Salg");
		openOrdreWindow(salg);
	}

	private void rundvisningAction() {
		Ordre rundvisning = controller.opretReservationsOrdre("Rundvisning", LocalTime.now(), LocalDate.now());
		openOrdreWindow(rundvisning);
	}

	private void udlejningsAction() {
		Ordre udlejning = controller.opretUdlejningsOrdre("Udlejning", LocalDate.now(), LocalDate.now());
		openOrdreWindow(udlejning);
	}
	
	private void openOrdreWindow(Ordre ordre) {
		OrdreWindue dia = new OrdreWindue("Opret " + ordre.getBeskrivelse(), ordre);
		dia.setOnCloseRequest(event -> controller.sletOrdre(ordre));
		dia.showAndWait();
	}
}
