package gui;

import application.controller.Controller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	private Controller controller;

	@Override
	public void init() {
		controller = Controller.getController();
		controller.loadStorage();
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Aarhus Bryghus");
		BorderPane pane = new BorderPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.setHeight(500);
		stage.setWidth(900);
		stage.show();
	}

	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tabOpretProduktOgGruppe = new Tab("Opret produkter og grupper");
		Tab tabOpretPrislisteOgPriser = new Tab("Opret prislister");
		Tab tabStatistik = new Tab("Vis statistiker");
		Tab tabOrdre = new Tab("Opret ordre");
		Tab tabAfslutBetalinger = new Tab("Afslut betalinger");

		OrdrePane opretOrdrePane = new OrdrePane();
		tabOrdre.setContent(opretOrdrePane);
		OpretProduktOgGruppePane opretProduktOgGruppePane = new OpretProduktOgGruppePane();
		tabOpretProduktOgGruppe.setContent(opretProduktOgGruppePane);
		StatistikPane statisikPane = new StatistikPane();
		tabStatistik.setContent(statisikPane);
		OpretPrislisteOgPriserPane opretPrislisterOgPriserPane = new OpretPrislisteOgPriserPane();
		tabOpretPrislisteOgPriser.setContent(opretPrislisterOgPriserPane);
		AfslutBetalingerPane afslutBetalingerPane = new AfslutBetalingerPane();
		tabAfslutBetalinger.setContent(afslutBetalingerPane);

		tabPane.getTabs().add(tabOrdre);
		tabPane.getTabs().add(tabStatistik);
		tabPane.getTabs().add(tabOpretProduktOgGruppe);
		tabPane.getTabs().add(tabOpretPrislisteOgPriser);
		tabPane.getTabs().add(tabAfslutBetalinger);

		tabOpretProduktOgGruppe.setOnSelectionChanged(event -> opretProduktOgGruppePane.updateView());	
		tabAfslutBetalinger.setOnSelectionChanged(event -> afslutBetalingerPane.opdaterListview());
		tabStatistik.setOnSelectionChanged(event -> statisikPane.updateView());
	}

	@Override
	public void stop() {
		controller.saveStorage();
	}

}
