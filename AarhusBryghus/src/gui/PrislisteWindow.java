package gui;


import application.controller.Controller;
import application.model.Pris;
import application.model.PrisListe;
import application.model.Produkt;
import application.model.ProduktGruppe;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class PrislisteWindow extends Stage{
	
	private PrisListe prisliste;
	private Controller controller;

	public PrislisteWindow(String title, PrisListe prisliste) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.prisliste = prisliste;

		controller = Controller.getController();

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

		if(prisliste != null) {
			opdaterPrisListe();
		}
		else {
			this.prisliste= controller.opretPrisliste("", "");
		}
	}


	public PrislisteWindow(String title) {
		this(title, null);
	}

	// ------------------------------------------------------------------

	private TextField txfName, txfBeskrivelse, txfPris, txfKlip;
	private Label lblError;
	private ListView<Produkt> lvwProdukter= new ListView<Produkt>();
	private ListView<ProduktGruppe> lvwProduktGruppe = new ListView<ProduktGruppe>();
	private ListView<Pris> lvwPriser= new ListView<Pris>();


	private void initContent(GridPane pane) {
		pane.setPadding((new Insets(10)));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblName = new Label("Navn");
		pane.add(lblName, 0, 0);

		txfName = new TextField();
		pane.add(txfName, 0, 1);
		txfName.setPrefWidth(200);

		Label lblBeskrivelse = new Label("Beskrivelse");
		pane.add(lblBeskrivelse, 1, 0);

		txfBeskrivelse = new TextField();
		pane.add(txfBeskrivelse, 1, 1);

		Label lblProduktgruppe = new Label("Produktgrupper");
		pane.add(lblProduktgruppe, 0, 2);
		
		pane.add(lvwProduktGruppe, 0, 3, 1, 8);
		lvwProduktGruppe.getItems().setAll(controller.getProduktGruppe());

		ChangeListener<ProduktGruppe> listenerPG = (ov, oldGruppe, newGruppe) -> selectedProdGruppeChanged(newGruppe);
		lvwProduktGruppe.getSelectionModel().selectedItemProperty().addListener(listenerPG);

		Label lblProdukter = new Label("Produkter");
		pane.add(lblProdukter, 1, 2);
		
		pane.add(lvwProdukter, 1, 3, 1, 8);

		Label lblPriser = new Label("Priser");
		pane.add(lblPriser, 3, 2);
		
		pane.add(lvwPriser, 3, 3, 1, 8);

		ChangeListener<Pris> listenerPris = (ov, oldPris, newPris) -> selectedPrisChanged(newPris);
		lvwPriser.getSelectionModel().selectedItemProperty().addListener(listenerPris);

		Label lblPris = new Label("Pris");
		pane.add(lblPris, 2, 3);

		txfPris = new TextField();
		pane.add(txfPris, 2, 4);

		Label lblKlip = new Label("Klip");
		pane.add(lblKlip, 2, 5);
		
		txfKlip = new TextField();
		pane.add(txfKlip, 2, 6);

		Button btnTilføj = new Button("Tilføj");
		pane.add(btnTilføj, 2, 7);
		btnTilføj.setOnAction(event -> this.tilfoejAction());
		btnTilføj.setMaxWidth(Double.MAX_VALUE);
		
		Button btnDelete = new Button("Slet");
		pane.add(btnDelete, 2, 8);
		btnDelete.setOnAction(event -> this.deleteAction());
		btnDelete.setMaxWidth(Double.MAX_VALUE);

		Button btnGem = new Button("Gem");
		pane.add(btnGem, 2, 9);
		btnGem.setOnAction(event -> this.okAction());
		btnGem.setMaxWidth(Double.MAX_VALUE);

		lblError = new Label();
		pane.add(lblError, 2, 1);
		lblError.setStyle("-fx-text-fill: red");
	}

	private void selectedProdGruppeChanged(ProduktGruppe pg) {
		if (pg != null) {
			lvwProdukter.getItems().setAll(pg.getProdukter());

		}
	}

	private void selectedPrisChanged(Pris pris) {
		if(pris!= null) {
			txfPris.setText(""+ pris.getPris());
			txfKlip.setText(""+pris.getKlip());
		}
	}

	private void tilfoejAction() {
		try {
			Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
			if(produkt==null) {
				lblError.setText("Venligst klik på et produkt");
			}
			else {
				double pris = Double.parseDouble(txfPris.getText()); 
				int klip = Integer.parseInt(txfKlip.getText());
				controller.opretPris(pris, klip, produkt, prisliste);
				lblError.setText("");
				opdater();
			}
		}catch(NumberFormatException e) {
			System.out.println(e.getMessage() + " Du må ikke indtaste bogstaver");
			lblError.setText("Du må ikke indtaste bogstaver");
		}
	}

	private void opdater() {
		lvwPriser.getItems().setAll(prisliste.getPriser());
		txfPris.clear();
		txfKlip.clear();
	}

	private void deleteAction() {
		Pris pris = lvwPriser.getSelectionModel().getSelectedItem();
		if(pris!=null) {
			controller.sletPris(pris);
			opdater();
		}
	}

	private void okAction() {
		String name = txfName.getText().trim();
		String beskrivelse = txfBeskrivelse.getText().trim();
		if (name.isEmpty() || beskrivelse.isEmpty()) {
			lblError.setText("Udfyld venligst begge felter");
		} else {
			controller.udpdatePrisliste(name, beskrivelse, prisliste);
		}
		this.close();
	}

	private void opdaterPrisListe() {
		txfName.setText(prisliste.getNavn());
		txfBeskrivelse.setText(prisliste.getBeskrivelse());
		lvwPriser.getItems().setAll(prisliste.getPriser());
	}

	public PrisListe getPrisliste() {
		return prisliste;
	}
}