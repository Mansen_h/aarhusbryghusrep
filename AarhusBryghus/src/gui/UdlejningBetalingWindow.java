package gui;

import application.controller.Controller;
import application.model.Betalingsmetode;
import application.model.Ordrelinje;
import application.model.UdlejningsOrdre;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class UdlejningBetalingWindow extends Stage {
	
	private UdlejningsOrdre udlejningsOrdre;
	private Controller controller;
	private ListView<Ordrelinje> lvwOrdrelinje = new ListView<>();
	private TextField txtFuld = new TextField();
	private TextField txtBetaltbeloeb = new TextField();
	private TextField txtKlip = new TextField();
	private ComboBox<Betalingsmetode> cbox = new ComboBox<>();
	private Button btnAflever = new Button("Aflever");
	private Button btnBetal = new Button("Betal");

	public UdlejningBetalingWindow(String title, UdlejningsOrdre udlejningsOrdre) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		controller = Controller.getController();
		this.udlejningsOrdre=udlejningsOrdre;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}
	
	private void initContent(GridPane pane) {
		pane.setPadding((new Insets(10)));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);
		
		Label lblOrdrelinje = new Label("Produkter til aflevering");
		pane.add(lblOrdrelinje, 0, 0);
		pane.add(lvwOrdrelinje, 0, 1, 1, 6);
		System.out.println(udlejningsOrdre.getOrdrelinjer());
		lvwOrdrelinje.getItems().setAll(udlejningsOrdre.getOrdrelinjer());
		
		Label lblfuld = new Label("Ikke brugte produkter:");
		pane.add(lblfuld, 1, 0);
		pane.add(txtFuld, 1, 1);

		pane.add(btnAflever, 1, 2);
		btnAflever.setOnAction(event -> afleverAction());
		
		Label lblbetalt = new Label("Manglende betaling");
		pane.add(lblbetalt, 2, 0);
		pane.add(txtBetaltbeloeb, 2, 1);
		
		Label lblKlip = new Label("Klip");
		pane.add(lblKlip, 2, 2);
		pane.add(txtKlip, 2, 3);
		
		Label lblBetaling = new Label("Betalingsmetode");
		pane.add(lblBetaling, 2, 4);
		pane.add(cbox, 2, 5);
		cbox.getItems().addAll(Betalingsmetode.values());
		
		pane.add(btnBetal, 2, 6);
		btnBetal.setOnAction(event->betalAction());	
	}
	
	private void afleverAction() {
		Ordrelinje ordreL = lvwOrdrelinje.getSelectionModel().getSelectedItem();
		int antal = Integer.parseInt(txtFuld.getText());
		if(ordreL!=null) {
			controller.opdaterOrdrelinje(ordreL, ordreL.getPris(), ordreL.getAntal()-antal);
			txtBetaltbeloeb.setText(""+udlejningsOrdre.beregnRestBeloebPris());
			lvwOrdrelinje.getItems().setAll(udlejningsOrdre.getOrdrelinjer());
		}
	}
	
	private void betalAction() {
		try {
			int klip = Integer.parseInt(txtKlip.getText().trim());
			double betal = Double.parseDouble(txtBetaltbeloeb.getText().trim());
			controller.opretBetaling(betal, klip, cbox.getValue(), udlejningsOrdre);
			controller.opdaterUdlejning(udlejningsOrdre, udlejningsOrdre.getBeskrivelse(), udlejningsOrdre.getUdlejningsDato(), udlejningsOrdre.getReturDato(), true);
			this.hide();
		} catch (Exception e) {
			//TODO
			System.out.println(e);
		}
	}
}