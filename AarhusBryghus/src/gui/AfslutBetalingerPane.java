package gui;

import application.controller.Controller;
import application.model.Ordre;
import application.model.ReservationsOrdre;
import application.model.UdlejningsOrdre;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;

public class AfslutBetalingerPane extends GridPane{
	
		private ListView<Ordre> lvwKommendeUdlejninger = new ListView<Ordre>();
		private ListView<Ordre> lvwIkkeAfleveretUdstyr = new ListView<Ordre>();
		private ListView<Ordre> lvwKommendeReservationer = new ListView<Ordre>();
		private ListView<Ordre> lvwIkkebetalteReservationer = new ListView<Ordre>();
		private ListView<Ordre> lvwManglendeBetaling = new ListView<Ordre>();
		private Button btnBetal = new Button("Aflever & betal");
		private Button btnBetal2 = new Button("Betal for reservation");
		private Button btnBetal3 = new Button("Betal og afslut ordre");
		private Label errorLabel1;
		private Label errorLabel2;
		private Label errorLabel3;

		private Controller controller;

		public AfslutBetalingerPane() {
			controller = Controller.getController();
			this.setPadding(new Insets(20));
			this.setHgap(20);
			this.setVgap(10); 
			this.setGridLinesVisible(false);
			initContent(); 
		}

		private void initContent() {
			Label lblkommendeUdlejninger = new Label("Kommende Udlejninger");
			this.add(lblkommendeUdlejninger, 0, 0);
			this.add(lvwKommendeUdlejninger, 0, 1, 1, 8);
			lvwKommendeUdlejninger.getItems().setAll(controller.kommendeUdlejninger());
			
			Label lbludstyr = new Label("Ikke afleveret udstyr");
			this.add(lbludstyr, 1, 0);
			this.add(lvwIkkeAfleveretUdstyr, 1, 1, 1, 8);
			lvwIkkeAfleveretUdstyr.getItems().setAll(controller.ikkeAfleveretUdstyr());
			
			this.add(lvwKommendeReservationer, 2, 1, 1, 8);
			lvwKommendeReservationer.getItems().setAll(controller.kommendeReservationer());
			Label lblreser = new Label("Kommende reservationer");
			this.add(lblreser, 2, 0);
			
			this.add(btnBetal, 1, 9);
			btnBetal.setOnAction(event -> betalAction());
			btnBetal.setMaxWidth(Double.MAX_VALUE);
			
			errorLabel1 = new Label();
			this.add(errorLabel1, 1, 10);
			errorLabel1.setStyle("-fx-text-fill: red");
			
			this.add(btnBetal2, 3, 9);
			btnBetal2.setOnAction(event -> betalReservation());
			btnBetal2.setMaxWidth(Double.MAX_VALUE);
			
			errorLabel2 = new Label();
			this.add(errorLabel2, 3, 10);
			errorLabel2.setStyle("-fx-text-fill: red");
			
			Label lblres = new Label("Ikke betalte reservationer");
			this.add(lblres, 3, 0);
			this.add(lvwIkkebetalteReservationer, 3, 1, 1, 8);
			lvwIkkebetalteReservationer.getItems().setAll(controller.ikkeBetalteReservationer());
			
			this.add(lvwManglendeBetaling, 4, 1, 1, 8);
			lvwManglendeBetaling.getItems().setAll(controller.ordreMangelBetaling());
			Label lblbetalM = new Label("Ordre med manglende betaling");
			this.add(lblbetalM, 4, 0);
			
			this.add(btnBetal3, 4, 9);
			btnBetal3.setOnAction(event ->afslutOrdre());
			btnBetal3.setMaxWidth(Double.MAX_VALUE);
			
			errorLabel3 = new Label();
			this.add(errorLabel3, 4, 10);
			errorLabel3.setStyle("-fx-text-fill: red");
		}
		
		private void betalAction() {
			UdlejningsOrdre uo = (UdlejningsOrdre) lvwIkkeAfleveretUdstyr.getSelectionModel().getSelectedItem();
			if(uo != null) {
				UdlejningBetalingWindow ubw = new UdlejningBetalingWindow("Betaling", uo);
				ubw.showAndWait();
				lvwIkkeAfleveretUdstyr.getItems().setAll(controller.ikkeAfleveretUdstyr());
				errorLabel1.setText("");
			} else {
				errorLabel1.setText("Der skal vælges en udlejning");
			}
		}
		
		private void betalReservation() {
			ReservationsOrdre ro = (ReservationsOrdre) lvwIkkebetalteReservationer.getSelectionModel().getSelectedItem();
			if(ro != null) {
				BetalingsWindow rbw = new BetalingsWindow("Betaling", ro);
				rbw.showAndWait();
				lvwIkkebetalteReservationer.getItems().setAll(controller.ikkeBetalteReservationer());
				errorLabel2.setText("");
			} else {
				errorLabel2.setText("Der skal vælges en reservation");
			}
		}
		
		private void afslutOrdre() {
			Ordre o = (Ordre) lvwManglendeBetaling.getSelectionModel().getSelectedItem();
			if(o != null) {
				BetalingsWindow bow = new BetalingsWindow("Betaling", o);
				bow.showAndWait();
				lvwManglendeBetaling.getItems().setAll(controller.ordreMangelBetaling());
				errorLabel3.setText("");
			} else {
				errorLabel3.setText("Der skal vælges en ordre");
			}
		}
		
		public void opdaterListview() {
			lvwIkkeAfleveretUdstyr.getItems().setAll(controller.ikkeAfleveretUdstyr());
			lvwIkkebetalteReservationer.getItems().setAll(controller.ikkeBetalteReservationer());
			lvwKommendeReservationer.getItems().setAll(controller.kommendeReservationer());
			lvwKommendeUdlejninger.getItems().setAll(controller.kommendeUdlejninger());
			lvwManglendeBetaling.getItems().setAll(controller.ordreMangelBetaling());
		}
	}