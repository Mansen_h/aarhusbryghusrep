package gui;

import application.controller.Controller;
import application.model.Produkt;
import application.model.ProduktGruppe;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class OpretProduktOgGruppePane extends GridPane {
	private Controller controller;

	public OpretProduktOgGruppePane() {
		controller = Controller.getController();
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		initContent();
	}

	private TextField txfNavnProduktgruppe, txfPantProduktgruppe, txfNavnProdukt, txfNrProdukt;
	private ListView<ProduktGruppe> lvwProduktgrupper = new ListView<ProduktGruppe>();
	private ListView<Produkt> lvwProdukter = new ListView<Produkt>();
	private Label errorLabelProduktgruppe;
	private Label errorLabelProdukt;

	private void initContent() {
		Label lblproduktgrupper = new Label("Produktgrupper");
		this.add(lblproduktgrupper, 0, 0);

		this.add(lvwProduktgrupper, 0, 1, 1, 8);
		ChangeListener<ProduktGruppe> listenerPG = (ov, oldGruppe,
				newGruppe) -> selectedProduktgruppeChanged(newGruppe);
		lvwProduktgrupper.getSelectionModel().selectedItemProperty().addListener(listenerPG);

		Label lblNavnProduktgruppe = new Label("Navn:");
		this.add(lblNavnProduktgruppe, 1, 0);

		txfNavnProduktgruppe = new TextField();
		this.add(txfNavnProduktgruppe, 1, 1);

		Label lblPantProduktgruppe = new Label("Pant:");
		this.add(lblPantProduktgruppe, 1, 2);

		txfPantProduktgruppe = new TextField("0");
		this.add(txfPantProduktgruppe, 1, 3);

		Button btnOpretProduktgruppe = new Button("Opret Gruppe");
		this.add(btnOpretProduktgruppe, 1, 4);
		btnOpretProduktgruppe.setMaxWidth(Double.MAX_VALUE);
		btnOpretProduktgruppe.setOnAction(event -> createProduktgruppeAction());

		Button btnOpdaterProduktgruppe = new Button("Opdater Gruppe");
		this.add(btnOpdaterProduktgruppe, 1, 5);
		btnOpdaterProduktgruppe.setMaxWidth(Double.MAX_VALUE);
		btnOpdaterProduktgruppe.setOnAction(event -> updateProduktgruppeAction());

		Button btnSletProduktgruppe = new Button("Slet Gruppe");
		this.add(btnSletProduktgruppe, 1, 6);
		btnSletProduktgruppe.setMaxWidth(Double.MAX_VALUE);
		btnSletProduktgruppe.setOnAction(event -> deleteProduktgruppeAction());

		errorLabelProduktgruppe = new Label();
		errorLabelProduktgruppe.setStyle("-fx-text-fill: red");
		this.add(errorLabelProduktgruppe, 1, 7);

		Label lblprodukter = new Label("Produkter");
		this.add(lblprodukter, 2, 0);

		this.add(lvwProdukter, 2, 1, 1, 8);
		ChangeListener<Produkt> listenerP = (ov, oldProdukt, newProdukt) -> selectedProduktChangend(newProdukt);
		lvwProdukter.getSelectionModel().selectedItemProperty().addListener(listenerP);

		Label lblNavnProdukt = new Label("Navn:");
		this.add(lblNavnProdukt, 3, 0);

		txfNavnProdukt = new TextField();
		this.add(txfNavnProdukt, 3, 1);

		Label lblNrProdukt = new Label("Nr:");
		this.add(lblNrProdukt, 3, 2);

		txfNrProdukt = new TextField();
		this.add(txfNrProdukt, 3, 3);

		Button btnOpretProdukt = new Button("Opret Produkt");
		this.add(btnOpretProdukt, 3, 4);
		btnOpretProdukt.setMaxWidth(Double.MAX_VALUE);
		btnOpretProdukt.setOnAction(event -> createProduktAction());

		Button btnOpdaterProdukt = new Button("Opdater Produkt");
		this.add(btnOpdaterProdukt, 3, 5);
		btnOpdaterProdukt.setMaxWidth(Double.MAX_VALUE);
		btnOpdaterProdukt.setOnAction(event -> updateProduktAction());

		Button btnSletProdukt = new Button("Slet Produkt");
		this.add(btnSletProdukt, 3, 6);
		btnSletProdukt.setMaxWidth(Double.MAX_VALUE);
		btnSletProdukt.setOnAction(event -> sletProduktAction());

		errorLabelProdukt = new Label();
		this.add(errorLabelProdukt, 3, 7);
		errorLabelProdukt.setStyle("-fx-text-fill: red");
		updateView();
	}

	private void createProduktgruppeAction() {
		try {
			String produktgruppeNavn = txfNavnProduktgruppe.getText().trim();
			double produktgruppePant = Double.parseDouble(txfPantProduktgruppe.getText());
			if (!produktgruppeNavn.isEmpty()) {
				controller.opretProduktgruppe(produktgruppeNavn, produktgruppePant);
				errorLabelProduktgruppe.setText("");
				updateLisViewProdgrupper();
			} else {
				errorLabelProduktgruppe.setText("Gruppe navn mangler");
			}
		} catch (NumberFormatException e) {
			errorLabelProduktgruppe.setText("Pant skal være et tal");
		}
	}

	private void updateProduktgruppeAction() {
		try {
			String produktgruppeNavn = txfNavnProduktgruppe.getText().trim();
			double produktgruppePant = Double.parseDouble(txfPantProduktgruppe.getText());
			ProduktGruppe produktgruppe = lvwProduktgrupper.getSelectionModel().getSelectedItem();
			if (!produktgruppeNavn.isEmpty()) {
				if (produktgruppe != null) {
					controller.updateProduktGruppe(produktgruppeNavn, produktgruppePant, produktgruppe);
					errorLabelProduktgruppe.setText("");
					updateLisViewProdgrupper();
				} else {
					errorLabelProduktgruppe.setText("Produktgruppe ikke valgt");
				}
			} else {
				errorLabelProduktgruppe.setText("Gruppe navn mangler");
			}
		} catch (NumberFormatException e) {
			errorLabelProduktgruppe.setText("Pant skal være et tal");
		}
	}

	private void deleteProduktgruppeAction() {
		ProduktGruppe produktgruppe = lvwProduktgrupper.getSelectionModel().getSelectedItem();
		if (produktgruppe != null) {
			controller.deleteProduktGruppe(produktgruppe);
			updateLisViewProdgrupper();
		} else {
			errorLabelProduktgruppe.setText("Produktgruppe ikke valgt");
		}
	}

	private void selectedProduktgruppeChanged(ProduktGruppe produktgruppe) {
		if (produktgruppe != null) {
			txfNavnProduktgruppe.setText(produktgruppe.getNavn());
			txfPantProduktgruppe.setText("" + produktgruppe.getPant());
			updateListViewProdukt();
		} else {
			txfPantProduktgruppe.clear();
			txfNavnProduktgruppe.setText("0");
		}
	}

	private void updateLisViewProdgrupper() {
		lvwProduktgrupper.getItems().setAll(controller.getProduktGruppe());
		txfNavnProduktgruppe.clear();
		txfPantProduktgruppe.setText("0");
	}

	// ---------------------------------------------------------------------------------------

	private void createProduktAction() {
		try {
			String produktNavn = txfNavnProdukt.getText().trim();
			int produktNr = Integer.parseInt(txfNrProdukt.getText());
			ProduktGruppe produktgruppe = lvwProduktgrupper.getSelectionModel().getSelectedItem();
			if (produktgruppe != null) {
				if (!produktNavn.isEmpty()) {
					controller.opretProdukt(produktNavn, produktNr, produktgruppe);
					updateListViewProdukt();
				} else {
					errorLabelProdukt.setText("Produkt navn ikke valgt");
				}
			} else {
				errorLabelProdukt.setText("Produktgruppe ikke valgt");
			}
		} catch (NumberFormatException e) {
			errorLabelProdukt.setText("Produkt nr skal være et tal");
		}
	}

	private void updateProduktAction() {
		try {
			String produktNavn = txfNavnProdukt.getText().trim();
			int produktNr = Integer.parseInt(txfNrProdukt.getText());
			ProduktGruppe produktgruppe = lvwProduktgrupper.getSelectionModel().getSelectedItem();
			Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
			if (produktgruppe != null) {
				if (produkt != null) {
					if (!produktNavn.isEmpty()) {
						controller.updateProdukt(produktNavn, produktNr, produktgruppe, produkt);
						updateListViewProdukt();
					} else {
						errorLabelProdukt.setText("Produkt navn ikke valgt");
					}
				} else {
					errorLabelProdukt.setText("Produkt ikke valgt");
				}
			} else {
				errorLabelProdukt.setText("Produktgruppe ikke valgt");
			}
		} catch (NumberFormatException e) {
			errorLabelProdukt.setText("Produkt nr skal være et tal");
		}
	}

	private void sletProduktAction() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		if (produkt != null) {
			controller.deleteProdukt(produkt);
			updateListViewProdukt();
		} else {
			errorLabelProdukt.setText("Produkt ikke valgt");
		}
	}

	private void selectedProduktChangend(Produkt produkt) {
		if (produkt != null) {
			txfNavnProdukt.setText(produkt.getNavn());
			txfNrProdukt.setText("" + produkt.getNr());
		} else {
			txfNavnProdukt.clear();
			txfNrProdukt.clear();
		}
	}

	private void updateListViewProdukt() {
		ProduktGruppe produktgruppe = lvwProduktgrupper.getSelectionModel().getSelectedItem();
		lvwProdukter.getItems().setAll(produktgruppe.getProdukter());
		txfNavnProdukt.clear();
		txfNrProdukt.clear();
	}

	// ------------------------------------------------------------------------

	public void updateView() {
		updateLisViewProdgrupper();
		txfNavnProdukt.clear();
		txfNrProdukt.clear();
		lvwProdukter.getItems().clear();
	}
}