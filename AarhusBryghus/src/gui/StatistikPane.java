package gui;

import java.time.LocalDate;

import application.controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class StatistikPane extends GridPane {

	private TextField txfSolgteKlip = new TextField();
	private TextField txfBrugteKlip = new TextField();
	private Controller controller;
	private TextArea txaDagsOrdre = new TextArea();
	private DatePicker dpDagsSalg = new DatePicker();
	private DatePicker dpStartDato = new DatePicker();
	private DatePicker dpSlutDato = new DatePicker();
	private Button btnVis = new Button("Vis");
	private Label lblError;

	public StatistikPane() {
		controller = Controller.getController();
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		initContent();
	}

	private void initContent() {
		Label lblDagensSalg = new Label("Dags salgs oversigt:");
		this.add(lblDagensSalg, 0, 3);

		this.add(dpDagsSalg, 0, 4);
		dpDagsSalg.setOnAction(event -> dpDagsSalgAction());
		dpDagsSalg.setValue(LocalDate.now());
		dpDagsSalg.setEditable(false);
		dpDagsSalgAction();

		this.add(txaDagsOrdre, 0, 5, 4, 1);
		txaDagsOrdre.setEditable(false);

		Label lblStart = new Label("Startdato for statistik:");
		this.add(lblStart, 0, 0);
		
		this.add(dpStartDato, 1, 0);
		dpStartDato.setValue(LocalDate.now().minusDays(30));
		dpStartDato.setEditable(false);
		
		this.add(dpSlutDato, 1, 1);
		dpSlutDato.setValue(LocalDate.now());
		dpSlutDato.setEditable(false);

		Label lblSlut = new Label("Slutdato for statistik:");
		this.add(lblSlut, 0, 1);

		Label lblsolgt = new Label("Solgte klippekort i den valgte periode");
		this.add(lblsolgt, 2, 0);
		this.add(txfSolgteKlip, 2, 1);
		txfSolgteKlip.setEditable(false);

		Label lblBrugt = new Label("Brugte klip i den valgte periode");
		this.add(lblBrugt, 3, 0);
		this.add(txfBrugteKlip, 3, 1);
		txfBrugteKlip.setEditable(false);

		this.add(btnVis, 1, 2);
		btnVis.setMaxWidth(Double.MAX_VALUE);
		btnVis.setOnAction(event->klipAction());
		
		lblError = new Label();
		this.add(lblError, 2, 2);
		lblError.setStyle("-fx-text-fill: red");
	}

	private void dpDagsSalgAction() {
		LocalDate picked = dpDagsSalg.getValue();
		txaDagsOrdre.setText(controller.ordreFraDatoString(picked));
	}
	
	private void klipAction() {
		LocalDate startDato = dpStartDato.getValue();
		LocalDate slutDato = dpSlutDato.getValue();
		if(startDato != null && slutDato != null) {
			if(!slutDato.isBefore(startDato)) {
				txfSolgteKlip.setText("" + controller.klippekortSolgt(startDato, slutDato));
				txfBrugteKlip.setText(""+ controller.klippekortBrugt(startDato, slutDato));
				lblError.setText("");
			}
			else {
				lblError.setText("Slut dato må ikke være før startdato");
			}
		}
	}
	
	public void updateView() {
		dpDagsSalgAction();
	}
}
