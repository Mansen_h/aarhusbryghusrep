package gui;

import application.controller.Controller;
import application.model.Betalingsmetode;
import application.model.Ordre;
import application.model.Ordrelinje;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class BetalingsWindow extends Stage {
	private Controller controller;
	private Ordre ordre;

	public BetalingsWindow(String title, Ordre ordre) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		controller = Controller.getController();
		this.ordre = ordre;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private TextField txfBeloebTilBet = new TextField();
	private TextArea txtaTotal = new TextArea();
	private ListView<Ordrelinje> lvwOrdrelinje = new ListView<Ordrelinje>();
	private ComboBox<Betalingsmetode> cBoxBetalingsmetode = new ComboBox<Betalingsmetode>();
	private Label lblBetaltBeløb;
	private Label lblErrorCol3;

	private void initContent(GridPane pane) {
		pane.setPadding((new Insets(10)));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		VBox col1 = new VBox();
		pane.add(col1, 0, 0);
		col1.setMaxWidth(300);
		col1.setPadding(new Insets(2));
		col1.setSpacing(5);

		Label lblOrdrelinje = new Label("Odrelinjer:");
		col1.getChildren().add(lblOrdrelinje);

		col1.getChildren().add(lvwOrdrelinje);
		lvwOrdrelinje.getItems().setAll(ordre.getOrdrelinjer());
		lvwOrdrelinje.setMaxHeight(300);

		col1.getChildren().add(txtaTotal);
		txtaTotal.setEditable(false);
		txtaTotal.setMaxHeight(45);

		// drop down label
		Label lblBetalingsmetoder = new Label("Betalingsmetode:");
		col1.getChildren().add(lblBetalingsmetoder);

		// dropdown betalingsmetoder
		cBoxBetalingsmetode.setItems(FXCollections.observableArrayList(Betalingsmetode.values()));
		col1.getChildren().add(cBoxBetalingsmetode);
		cBoxBetalingsmetode.getSelectionModel().select(Betalingsmetode.KONTANT);
		cBoxBetalingsmetode.setOnAction(event -> cBoxBetalingsmetodeAction());
		cBoxBetalingsmetode.setMaxWidth(Double.MAX_VALUE);

		// beløb til betaling
		lblBetaltBeløb = new Label("Indtast betalt beløb (Kr):");
		col1.getChildren().add(lblBetaltBeløb);

		// beløb til betaling txf
		col1.getChildren().add(txfBeloebTilBet);

		Button btnBetal = new Button("Betal");
		col1.getChildren().add(btnBetal);
		btnBetal.setMaxWidth(Double.MAX_VALUE);
		btnBetal.setOnAction(event -> betalAction());

		Button btnAfbryd = new Button("Afbryd");
		col1.getChildren().add(btnAfbryd);
		btnAfbryd.setMaxWidth(Double.MAX_VALUE);
		btnAfbryd.setOnAction(event -> close());

		lblErrorCol3 = new Label();
		col1.getChildren().add(lblErrorCol3);
		lblErrorCol3.setStyle("-fx-text-fill: red");
		txaTotalUdregning();
	}

	private void cBoxBetalingsmetodeAction() {
		Betalingsmetode valgtMetode = cBoxBetalingsmetode.getValue();
		if (valgtMetode == Betalingsmetode.KLIPPEKORT) {
			lblBetaltBeløb.setText("Indtast betalt beløb (Klip):");
		} else {
			lblBetaltBeløb.setText("Indtast betalt beløb (Kr):");
		}
	}
	
	private void txaTotalUdregning() {
		String txtString = "Total:\t\t";
		double ordreTotalBeloeb = ordre.beregnTotalBeloebPris();
		int ordreTotalKlip = ordre.beregnTotalKlipPris();
		double ordrePant = ordre.beregnPantPris();
		txtString += ordreTotalBeloeb + " kr ";

		if (ordreTotalKlip > 0) {
			txtString += "eller " + ordreTotalKlip + " klip";
		}
		if (ordrePant > 0) {
			txtString += ", " + ordrePant + " pant";
		}
		txtString += "\nRestbeløb:\t";
		txtString += ordre.beregnRestBeloebPris() + " kr ";
		int ordreRestKlip = ordre.beregnRestKlipPris();
		if (ordreRestKlip > 0) {
			txtString += "eller " + ordreRestKlip + " klip";
		}
		txtaTotal.setText(txtString);
	}
	
	private void betalAction() {
		try {
			Betalingsmetode betalingsmetode = cBoxBetalingsmetode.getValue();
			if (betalingsmetode == Betalingsmetode.KLIPPEKORT) {
				int antalKlip = Integer.parseInt(txfBeloebTilBet.getText().trim());
				controller.opretBetaling(0, antalKlip, betalingsmetode, ordre);
			} else {
				double betaltbeloeb = Double.parseDouble(txfBeloebTilBet.getText().trim());
				controller.opretBetaling(betaltbeloeb, 0, betalingsmetode, ordre);
			}
			super.close();
		} catch (NumberFormatException e) {
			lblErrorCol3.setText("Beløb betalt skal være et tal");
		}
	}
}