package gui;

import application.controller.Controller;
import application.model.PrisListe;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;

public class OpretPrislisteOgPriserPane extends GridPane {

	private ListView<PrisListe> lvwPrislister = new ListView<PrisListe>();

	private Controller controller;

	public OpretPrislisteOgPriserPane() {
		controller = Controller.getController();
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10); 
		this.setGridLinesVisible(false);
		initContent(); 
	}

	private void initContent() {
		Label lblPrislister = new Label("Prislister");
		this.add(lblPrislister, 0, 0);

		this.add(lvwPrislister, 0, 1, 1, 8);		
		
		Button btnOpret = new Button("Opret");
		this.add(btnOpret, 1, 1);
		btnOpret.setOnAction(event -> createPrislisteAction());
		btnOpret.setMaxWidth(Double.MAX_VALUE);
		
		Button btnOpdater = new Button("Opdater");
		this.add(btnOpdater, 1, 2);
		btnOpdater.setOnAction(event -> updatePrislisteAction());
		btnOpdater.setMaxWidth(Double.MAX_VALUE);
		
		Button btnDelete = new Button("Slet");
		this.add(btnDelete, 1, 3);
		btnDelete.setOnAction(event -> deletePrislisteAction());
		btnDelete.setMaxWidth(Double.MAX_VALUE);
		
		updateView();
	}


	// ---------------------------------------------------------------------------------------


	private void createPrislisteAction() {	
		PrislisteWindow op = new PrislisteWindow("Opret Prisliste");
		op.setOnCloseRequest(event -> controller.deletePrisliste(op.getPrisliste()));
		op.showAndWait();
		updateView();
	}

	private void updatePrislisteAction() {
		PrisListe prisliste = lvwPrislister.getSelectionModel().getSelectedItem();
		if(prisliste != null) {
			PrislisteWindow op = new PrislisteWindow("Opdater Prisliste", prisliste);
			op.showAndWait();
			updateView();
		}
	}
	
	private void deletePrislisteAction() {
		PrisListe prisliste = lvwPrislister.getSelectionModel().getSelectedItem();
		if(prisliste != null) {
			controller.deletePrisliste(prisliste);
			updateView();
		}
	}
	
	public void updateView() {
		lvwPrislister.getItems().setAll(controller.getPrisliste());
	}
}
