package gui;

import java.time.LocalDate;
import java.time.LocalTime;
import application.controller.Controller;
import application.model.Betalingsmetode;
import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Pris;
import application.model.PrisListe;
import application.model.ProduktGruppe;
import application.model.ReservationsOrdre;
import application.model.UdlejningsOrdre;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class OrdreWindue extends Stage {

	public OrdreWindue(String title, Ordre ordre) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.ordre = ordre;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
		this.setHeight(600);
		this.setWidth(900);
	}

	// ------------------------------------------------------------------

	private Controller controller;
	private Ordre ordre;
	private Label lblErrorCol2;
	private Label lblErrorCol3;
	private Label lblBetaltBeløb;
	private TextField txfAntal = new TextField();
	private TextField txfBeloebTilBet = new TextField();
	private TextField txfResTid = new TextField();
	private TextField txfRabat = new TextField();
	private TextArea txtaTotal = new TextArea();
	private TextArea txtaNyLinjeSum = new TextArea();
	private ListView<ProduktGruppe> lvwProduktGruppe = new ListView<ProduktGruppe>();
	private ListView<Pris> lvwPris = new ListView<Pris>();
	private ListView<Ordrelinje> lvwOrdrelinje = new ListView<Ordrelinje>();
	private ComboBox<PrisListe> cBoxPrislist = new ComboBox<PrisListe>();
	private ComboBox<Betalingsmetode> cBoxBetalingsmetode = new ComboBox<Betalingsmetode>();
	private DatePicker dpStart;
	private DatePicker dpSlut;
	private DatePicker dpResDato;
	private ToggleGroup tglG = new ToggleGroup();
	private ToggleButton tbFastRabat = new ToggleButton("Fast rabat");
	private ToggleButton tbProcentRabat = new ToggleButton("Procent rabat");
	private Button btnTilfoej;

	private void initContent(GridPane pane) {
		controller = Controller.getController();
		pane.setPadding((new Insets(10)));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblPrisliste = new Label("Prislister:");
		pane.add(lblPrisliste, 0, 0);

		cBoxPrislist.setItems(FXCollections.observableArrayList(controller.getPrisliste()));
		pane.add(cBoxPrislist, 0, 1);
		cBoxPrislist.setOnAction(event -> cBoxPrisListeAction());

		VBox col1 = new VBox();
		pane.add(col1, 0, 2);
		col1.setMaxWidth(300);
		col1.setPadding(new Insets(2));
		col1.setSpacing(5);

		ChangeListener<ProduktGruppe> listenerPG = (ov, oldGruppe, newGruppe) -> selectedProdGruppeChanged(newGruppe);
		lvwProduktGruppe.getSelectionModel().selectedItemProperty().addListener(listenerPG);
		lvwProduktGruppe.setDisable(true);

		// lvwProduktGruppe.setMaxWidth(300);

		VBox col2 = new VBox();
		pane.add(col2, 1, 2);
		col2.setMaxWidth(300);
		col2.setPadding(new Insets(2));
		col2.setSpacing(5);

		Label lblProdukt = new Label("Produkter:");
		col2.getChildren().add(lblProdukt);// pane.add(lblProdukt, 1, 2);

		col2.getChildren().add(lvwPris);// pane.add(lvwPris, 1, 3, 2, 8);
		ChangeListener<Pris> listenerPris = (ov, oldPris, newPris) -> selectedPrisChanged(newPris);
		lvwPris.getSelectionModel().selectedItemProperty().addListener(listenerPris);
		lvwPris.setDisable(true);

		// lvwPris.setMaxWidth(300);

		Label lblAntal = new Label("Antal:");
		col2.getChildren().add(lblAntal);// pane.add(lblAntal, 1, 11);

		col2.getChildren().add(txfAntal);// pane.add(txfAntal, 2, 11);
		txfAntal.setOnKeyTyped(event -> txtAreaSumUdregning());
		txfAntal.setDisable(true);

		col2.getChildren().add(txtaNyLinjeSum);// pane.add(txtabeløb, 1, 12, 2, 1);
		txtaNyLinjeSum.setEditable(false);
		txtaNyLinjeSum.setMinHeight(45);
		txtaNyLinjeSum.setDisable(true);
		txtaNyLinjeSum.setWrapText(true);

		tbFastRabat.setMaxWidth(Double.MAX_VALUE);
		tbFastRabat.setUserData(true);
		tbFastRabat.setDisable(true);

		tbProcentRabat.setMaxWidth(Double.MAX_VALUE);
		tbProcentRabat.setUserData(false);
		tbProcentRabat.setDisable(true);

		HBox hBoxRabat = new HBox();
		hBoxRabat.getChildren().add(tbFastRabat);
		hBoxRabat.getChildren().add(tbProcentRabat);
		col2.getChildren().add(hBoxRabat);// pane.add(hBox, 1, 13, 2, 1);
		hBoxRabat.setMaxWidth(Double.MAX_VALUE);
		hBoxRabat.setSpacing(5);

		tglG.getToggles().add(tbProcentRabat);
		tglG.getToggles().add(tbFastRabat);

		tbProcentRabat.setOnAction(event -> tglBtnRabatAction());
		tbFastRabat.setOnAction(event -> tglBtnRabatAction());

		col2.getChildren().add(txfRabat);// pane.add(txfrabatBeloeb, 1, 14, 2, 1);
		txfRabat.setDisable(true);

		btnTilfoej = new Button("Tilføj til ordre");
		col2.getChildren().add(btnTilfoej);// pane.add(btnTilfoej, 1, 15, 2, 1);
		btnTilfoej.setMaxWidth(Double.MAX_VALUE);
		btnTilfoej.setOnAction(event -> AddToOrdrelinjeAction());
		btnTilfoej.setDisable(true);

		lblErrorCol2 = new Label("");
		lblErrorCol2.setStyle("-fx-text-fill: red");
		col2.getChildren().add(lblErrorCol2);

		VBox col3 = new VBox();
		pane.add(col3, 2, 2);
		col3.setMaxWidth(300);
		col3.setPadding(new Insets(2));
		col3.setSpacing(5);

		Label lblOrdrelinje = new Label("Odrelinjer:");
		col3.getChildren().add(lblOrdrelinje);// pane.add(lblOrdrelinje, 3, 2, 1, 1);

		col3.getChildren().add(lvwOrdrelinje);// pane.add(lvwOrdrelinje, 3, 3, 1, 6);

		col3.getChildren().add(txtaTotal);// pane.add(txtaSum, 3, 9);
		txtaTotal.setEditable(false);
		txtaTotal.setMinHeight(45);

		// drop down label
		Label lblBetalingsmetoder = new Label("Betalingsmetode:");
		col3.getChildren().add(lblBetalingsmetoder);// pane.add(lblBetalingsmetoder, 3, 10);

		// dropdown betalingsmetoder
		cBoxBetalingsmetode.setItems(FXCollections.observableArrayList(Betalingsmetode.values()));
		col3.getChildren().add(cBoxBetalingsmetode);// pane.add(cBoxBetalingsmetode, 3, 11);
		cBoxBetalingsmetode.getSelectionModel().select(Betalingsmetode.KONTANT);
		cBoxBetalingsmetode.setOnAction(event -> cBoxBetalingsmetodeAction());
		cBoxBetalingsmetode.setMaxWidth(Double.MAX_VALUE);

		// beløb til betaling
		lblBetaltBeløb = new Label("Indtast betalt beløb (Kr):");
		col3.getChildren().add(lblBetaltBeløb);// pane.add(lblBetaltBeløb, 3, 12);

		// beløb til betaling txf
		col3.getChildren().add(txfBeloebTilBet);// pane.add(txfBeloebTilBet, 3, 13);
		// txfBeloebTilBet.setMaxWidth(300);

		Button btnBetal = new Button("Betal");
		col3.getChildren().add(btnBetal);// pane.add(btnBetal, 3, 14, 1, 1);
		btnBetal.setMaxWidth(Double.MAX_VALUE);

		Button btnAfbryd = new Button("Afbryd");
		col3.getChildren().add(btnAfbryd);// pane.add(btnAfbryd, 3, 15, 1, 1);
		btnAfbryd.setMaxWidth(Double.MAX_VALUE);
		btnAfbryd.setOnAction(event -> close());

		lblErrorCol3 = new Label();
		col3.getChildren().add(lblErrorCol3);
		lblErrorCol3.setStyle("-fx-text-fill: red");
		
		if (ordre instanceof UdlejningsOrdre) {
			Label lblDatoStart = new Label("Udlejningsdato start:");
			col1.getChildren().add(lblDatoStart);// pane.add(lblDatoStart, 0, 12);

			dpStart = new DatePicker();
			col1.getChildren().add(dpStart);// pane.add(dpStart, 0, 13);
			dpStart.setValue(LocalDate.now());
			dpStart.setMaxWidth(Double.MAX_VALUE);

			Label lblDato = new Label("Udlejningsdato slut:");
			col1.getChildren().add(lblDato);// pane.add(lblDato, 0, 14);

			dpSlut = new DatePicker();
			col1.getChildren().add(dpSlut);// pane.add(dpSlut, 0, 15);
			dpSlut.setValue(LocalDate.now());
			dpSlut.setMaxWidth(Double.MAX_VALUE);

			Label lblProduktgruppe = new Label("Produkt Gruppe:");
			col1.getChildren().add(lblProduktgruppe);// pane.add(lblProduktgruppe, 0, 2);

			col1.getChildren().add(lvwProduktGruppe);// pane.add(lvwProduktGruppe, 0, 3, 1, 9);

			btnBetal.setOnAction(event -> betalUdlejningAction());
		} else if (ordre instanceof ReservationsOrdre) {
			Label lblResTid = new Label("Reservations Tidspunkt:");
			col1.getChildren().add(lblResTid);// pane.add(lblResTid, 0, 12);

			col1.getChildren().add(txfResTid);// pane.add(txfResTid, 0, 13);
			txfResTid.setMaxWidth(Double.MAX_VALUE);

			Label lblResDato = new Label("Reservations Dato:");
			col1.getChildren().add(lblResDato);// pane.add(lblResDato, 0, 14);

			dpResDato = new DatePicker();
			col1.getChildren().add(dpResDato);// pane.add(dpResDato, 0, 15);
			dpResDato.setMaxWidth(Double.MAX_VALUE);
			dpResDato.setValue(LocalDate.now());

			Label lblProduktgruppe = new Label("Produkt Gruppe:");
			col1.getChildren().add(lblProduktgruppe);// pane.add(lblProduktgruppe, 0, 2);

			col1.getChildren().add(lvwProduktGruppe);// pane.add(lvwProduktGruppe, 0, 3, 1, 9);

			btnBetal.setOnAction(event -> betalReservationAction());
		} else {
			Label lblProduktgruppe = new Label("Produkt Gruppe:");
			col1.getChildren().add(lblProduktgruppe);// pane.add(lblProduktgruppe, 0, 2);

			col1.getChildren().add(lvwProduktGruppe);// pane.add(lvwProduktGruppe, 0, 3, 1, 13);

			btnBetal.setOnAction(event -> betalAction());
		}
		txaTotalUdregning();
	}

	private void betalUdlejningAction() {
		LocalDate startDato = dpStart.getValue();
		LocalDate slutDato = dpSlut.getValue();
		if (startDato != null && slutDato != null) {
			if (!slutDato.isBefore(startDato) && !startDato.isBefore(LocalDate.now())) {
				controller.opdaterUdlejning((UdlejningsOrdre) ordre, ordre.getBeskrivelse(), startDato, slutDato, false);
				betalAction();
			} else {
				lblErrorCol3.setText("Afleverings dato kan ikke være før udlejnings dato");
			}
		}
	}

	private void betalReservationAction() {
		try {
			LocalDate dato = dpResDato.getValue();
			LocalTime tidspunkt = LocalTime.parse(txfResTid.getText().trim());
			if (dato != null && tidspunkt != null) {
				System.out.println((dato.equals(LocalDate.now()) && !tidspunkt.isBefore(LocalTime.now())));
				if (dato.isAfter(LocalDate.now()) || (dato.equals(LocalDate.now()) && !tidspunkt.isBefore(LocalTime.now()))) {
					controller.opdaterReservation((ReservationsOrdre) ordre, ordre.getBeskrivelse(), tidspunkt, dato);
					betalAction();
				} else {
					lblErrorCol3.setText("Reservations skal være efter " + LocalDate.now() + " kl: " + LocalTime.now().getHour() + ":" + LocalTime.now().getMinute());
				}
			} else {
				lblErrorCol3.setText("Vælg dato");
			}
		} catch (Exception e) {
			lblErrorCol3.setText("Tidspunkt skal skrives som: tt:mm");
		}
	}

	private void selectedProdGruppeChanged(ProduktGruppe pg) {
		PrisListe pr = cBoxPrislist.getSelectionModel().getSelectedItem();
		if (pg != null && pr != null) {
			lvwPris.getItems().setAll(controller.produktPaaPrisListeIGruppe(pr, pg));
			lvwPris.setDisable(false);
		} else {
			lvwPris.getItems().clear();
			lvwPris.setDisable(true);
		}
	}

	private void selectedPrisChanged(Pris p) {
		if (p != null) {
			txfAntal.setText("" + 1);
			txfAntal.setDisable(false);
			tbFastRabat.setDisable(false);
			tbProcentRabat.setDisable(false);
			btnTilfoej.setDisable(false);
			tglBtnRabatAction();
			txtAreaSumUdregning();
		} else {
			txfAntal.setText("");
			txfAntal.setDisable(true);
			txtaNyLinjeSum.setText("");
			txtaNyLinjeSum.setDisable(true);
			btnTilfoej.setDisable(true);
			tbFastRabat.setDisable(true);
			tbProcentRabat.setDisable(true);
			txfRabat.setDisable(true);
		}
	}

	private void cBoxPrisListeAction() {
		PrisListe pl = cBoxPrislist.getSelectionModel().getSelectedItem();
		if (pl != null) {
			lvwProduktGruppe.getItems().setAll(controller.produktGrupperPaaPrisListe(pl));
			lvwProduktGruppe.setDisable(false);
		} else {
			lvwProduktGruppe.getItems().clear();
			lvwProduktGruppe.setDisable(true);
		}
	}

	private void AddToOrdrelinjeAction() {
		try {
			Pris p = lvwPris.getSelectionModel().getSelectedItem();
			int antalProdukter = Integer.parseInt(txfAntal.getText().trim());
			if (p != null && antalProdukter != 0) {
				double rabat = 0;
				if (tglG.getSelectedToggle() != null) {
					rabat = Double.parseDouble(txfRabat.getText().trim());
				}
				Ordrelinje ordrelinje = controller.opretOrdreLinje(antalProdukter, p, ordre);
				txfAntal.clear();
				lvwPris.getSelectionModel().select(null);
				if (tglG.getSelectedToggle() != null && rabat > 0) {
					boolean fast = (boolean) tglG.getSelectedToggle().getUserData();
					if (fast) {
						controller.createFastRabat(ordrelinje, rabat);
					} else {
						controller.createProcentRabat(ordrelinje, rabat);
					}
				}
				lvwOrdrelinje.getItems().setAll(ordre.getOrdrelinjer());
				txtaNyLinjeSum.clear();
				txtaNyLinjeSum.setDisable(true);
				txaTotalUdregning();
				lblErrorCol2.setText("");
			}
		} catch (NumberFormatException e) {
			lblErrorCol2.setText("Antal og rabat skal være tal");
		}
	}

	private void cBoxBetalingsmetodeAction() {
		Betalingsmetode valgtMetode = cBoxBetalingsmetode.getValue();
		if (valgtMetode == Betalingsmetode.KLIPPEKORT) {
			lblBetaltBeløb.setText("Indtast betalt beløb (Klip):");
		} else {
			lblBetaltBeløb.setText("Indtast betalt beløb (Kr):");
		}
	}

	private void betalAction() {
		try {
			Betalingsmetode betalingsmetode = cBoxBetalingsmetode.getValue();
			if (ordre.getOrdrelinjer().size() < 1) {
				lblErrorCol3.setText("Ordren er tom");
			} else if (betalingsmetode == Betalingsmetode.KLIPPEKORT) {
				int antalKlip = Integer.parseInt(txfBeloebTilBet.getText().trim());
				controller.opretBetaling(0, antalKlip, betalingsmetode, ordre);
			} else {
				double betaltbeloeb = Double.parseDouble(txfBeloebTilBet.getText().trim());
				controller.opretBetaling(betaltbeloeb, 0, betalingsmetode, ordre);
			}
			super.close();
		} catch (NumberFormatException e) {
			lblErrorCol3.setText("Beløb betalt skal være et tal");
		}
	}

	private void tglBtnRabatAction() {
		if (tbFastRabat.isSelected() || tbProcentRabat.isSelected()) {
			txfRabat.setDisable(false);
		} else {
			txfRabat.setDisable(true);
			txfRabat.clear();
		}
	}

	private void txaTotalUdregning() {
		String txtString = "Total:\t\t";
		double ordreTotalBeloeb = ordre.beregnTotalBeloebPris();
		int ordreTotalKlip = ordre.beregnTotalKlipPris();
		double ordrePant = ordre.beregnPantPris();
		txtString += ordreTotalBeloeb + " kr ";

		if (ordreTotalKlip > 0) {
			txtString += "eller " + ordreTotalKlip + " klip";
		}
		if (ordrePant > 0) {
			txtString += ", " + ordrePant + " pant";
		}
		txtString += "\nRestbeløb:\t";
		txtString += ordre.beregnRestBeloebPris() + " kr ";
		int ordreRestKlip = ordre.beregnRestKlipPris();
		if (ordreRestKlip > 0) {
			txtString += "eller " + ordreRestKlip + " klip";
		}
		txtaTotal.setText(txtString);
	}

	private void txtAreaSumUdregning() {
		try {
			int antal = Integer.parseInt(txfAntal.getText());
			Pris pris = lvwPris.getSelectionModel().getSelectedItem();
			String txtString = "";
			if (pris != null) {
				txtString += antal + " x " + pris.getProdukt().getNavn() + ": ";
				txtString += priceStringAmountFormat(pris, 1);
				if (antal > 1) {
					txtString += "\nSum: " + priceStringAmountFormat(pris, antal);
				}
				txtaNyLinjeSum.setText(txtString);
				txtaNyLinjeSum.setDisable(false);
			}
		} catch (NumberFormatException e) {
			System.out.println("Textfeltet var tomt eller ikke et tal, afbryd opdatering af textarea.");
			System.out.println(e);
		}
	}

	private String priceStringAmountFormat(Pris pris, int antal) {
		String txtString = "";
		if (pris.getPris() > 0) {
			txtString += (antal * pris.getPris()) + " kr ";
		}
		if (pris.getKlip() > 0) {
			txtString += ", " + (antal * pris.getKlip()) + " klip";
		}
		if (pris.getPant() > 0) {
			txtString += ", " + (antal * pris.getPant()) + " pant";
		}
		return txtString;
	}

	@Override
	public void close() {
		controller.sletOrdre(ordre);
		super.close();

	}
}